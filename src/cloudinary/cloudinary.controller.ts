import {
    Controller,
    Post,
    HttpStatus,
    UploadedFile,
    UploadedFiles,
    UseInterceptors,
    ParseFilePipe,
    MaxFileSizeValidator,
    FileTypeValidator,
    UseGuards
  } from '@nestjs/common';
import { CloudinaryService } from './cloudinary.service';
import { FileInterceptor,FileFieldsInterceptor } from '@nestjs/platform-express';
import { Express } from 'express';
import { formatResponse } from '../utils/helpers/formatResponse';
import { MAX_FILE_SIZE } from '../utils/file/constants.file';
import { JwtAuthGuard } from 'src/modules/auth/guard/jwt-auth.guard';
import RoleGuard from 'src/modules/auth/guard/auth.guard';
import { UserRole } from 'src/utils/enum/user-role.enum';

@Controller('cloudy')
export class CloudinaryController {
    constructor(private readonly cloudinaryService: CloudinaryService) {}

    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.USER, UserRole.ADMIN))
    @Post('upload-avatar')
    @UseInterceptors(FileInterceptor('avatar'))
    async uploadAvatar(
        @UploadedFile(
        new ParseFilePipe({
            validators: [
                new MaxFileSizeValidator({ maxSize: MAX_FILE_SIZE }),
                new FileTypeValidator({ fileType: /(jpg|jpeg|png|gif)$/ })
            ],
          }),
        )
        file: Express.Multer.File,
        ){
        const linkAvatar = await this.cloudinaryService.uploadImage(file);
        return formatResponse(
            HttpStatus.OK,
            'Upload avatar success!',
            {url: linkAvatar.url, avatar_name: linkAvatar.public_id},
        )
    }

    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN))
    @Post('products/upload-image')
    @UseInterceptors(FileFieldsInterceptor([
    { name: 'images', maxCount: 5}
    ]))
    async uploadImagesOfProduct(@UploadedFiles(
    ) files: { images?: Express.Multer.File[]}) {
        try {
            if(!files.images) {
                return formatResponse(
                    HttpStatus.BAD_REQUEST,
                    'File is required'
                )
            }
            const result = await Promise.all(files.images.map(async (file: Express.Multer.File) =>{
                return new Promise(async(resolve,reject) => {
                    if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
                        reject('only image files allowed');
                    }
                    if(file.size > MAX_FILE_SIZE){
                        reject('Please upload image has size less than 1Mb');
                    }
                    const linkAvatar = await this.cloudinaryService.uploadImage(file);
                    resolve({url: linkAvatar.url, avatar_name: linkAvatar.public_id});
                })
            }))
            return formatResponse(
                HttpStatus.OK,
                'Upload images successfully!',
                result,
            )
        } catch (error) { 
            return formatResponse(
            HttpStatus.BAD_REQUEST,
            error?.message||error
            )
        }
    }
}

