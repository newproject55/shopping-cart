import { Module } from "@nestjs/common";
import { DatabaseModule } from "./database/database.module";
import { TokensModule } from "./modules/tokens/tokens.module";
import { VoucherModule } from "./modules/voucher/voucher.module";
import { PaymentMethodModule } from "./modules/payment-method/payment-method.module";
import { OrdersModule } from "./modules/orders/orders.module";
import { ReviewsModule } from "./modules/reviews/reviews.module";
import { ProductsOfOrderModule } from "./modules/products-of-order/products-of-order.module";
import { ProductsOfCartModule } from "./modules/products-of-cart/products-of-cart.module";
import { ProductsModule } from "./modules/products/products.module";
import { ImagesProductModule } from "./modules/images-product/images-product.module";
import { ProductsOfCategoryModule } from "./modules/products-of-category/products-of-category.module";
import { CategoryModule } from "./modules/category/category.module";
import { UsersModule } from "./modules/users/users.module";
import { VouchersOfOrderModule } from "./modules/vouchers-of-order/vouchers-of-order.module";
import { CloudinaryModule } from "./cloudinary/cloudinary.module";
import { AuthModule } from "./modules/auth/auth.module";

@Module({
  imports: [
    DatabaseModule,
    TokensModule,
    VoucherModule,
    PaymentMethodModule,
    OrdersModule,
    ReviewsModule,
    ProductsOfOrderModule,
    ProductsOfCartModule,
    ProductsModule,
    ImagesProductModule,
    ProductsOfCategoryModule,
    CategoryModule,
    UsersModule,
    VouchersOfOrderModule,
    CloudinaryModule,
    AuthModule
  ],
  controllers: [],
  providers: []
})
export class AppModule {}
