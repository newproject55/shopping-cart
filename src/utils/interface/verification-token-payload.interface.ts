export interface VerificationTokenPayload {
  email?: string;
  code?: string;
}
