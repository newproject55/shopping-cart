import { Request } from 'express';
import users from 'src/modules/users/entity/users.entity';

export interface RequestWithUser extends Request {
    user: users;
}
