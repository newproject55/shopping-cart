import { UserRole } from "../enum/user-role.enum";

export interface TokenPayload {
    id: number;
    email: string;
    roles: UserRole[];
}

export interface JwtSign {
    access_token: string;
    refresh_token: string;
}