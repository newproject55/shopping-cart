export interface voucherOfOrder {
    voucher_name: string;
    value: number;
    orders: object;
    vouchers: object;
}