export enum SortFieldUsersEnum {
    ID = 'id',
    NAME = 'username',
    EMAIL = 'email',
    PHONE = 'phone_number',
    CREATED_AT = 'createdAt',
    UPDATED_AT = 'updatedAt',
  }