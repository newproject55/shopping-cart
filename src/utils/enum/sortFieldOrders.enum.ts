export enum SortFieldOrdersEnum {
    ID = 'id',
    PAIDED = 'paided',
    PAYMENT = 'payment_method',
    STATUS = 'status',
    CREATED_AT = 'createdAt',
    UPDATED_AT = 'updatedAt',
  }