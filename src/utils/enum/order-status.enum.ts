export enum OrderStatus {
    PROCESSING = "Processing",
    SHIPPING = "Shipping",
    COMPLETED = "Completed"
}