export enum SortFieldProductsEnum {
    ID = 'id',
    NAME = 'title',
    BRAND = 'brand',
    PRICE = 'price',
    DISCOUNT_PERCENT = 'discount_percent',
    RATE = 'rate_avg_review',
    CREATED_AT = 'createdAt',
    UPDATED_AT = 'updatedAt',
  }