export const formatResponse = (
    statusCode: number,
    message: string,
    data?: any,
) => ({
    statusCode,
    message,
    data,
});
