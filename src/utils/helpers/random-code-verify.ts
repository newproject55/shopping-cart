export const randomNumberCode = (length: number) => {
    const code = Math.floor(Math.random() * Math.pow(10, length));
    return code.toString();
  }