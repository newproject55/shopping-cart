export const generatePassword = () => {
    const newPassword = Math.random().toString(36).slice(2,10);
    return newPassword;
}