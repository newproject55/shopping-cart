export const removeVietnameseAccent = (char: string): string => {
  char = char.toLowerCase();
  char = char.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
  char = char.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
  char = char.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
  char = char.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
  char = char.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
  char = char.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
  char = char.replace(/đ/g, 'd');
  // Some systems encode vietnamese combining accent as individual utf-8 characters
  char = char.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ''); // Huyền sắc hỏi ngã nặng
  char = char.replace(/\u02C6|\u0306|\u031B/g, ''); // Â, Ê, Ă, Ơ, Ư
  return char;
};
