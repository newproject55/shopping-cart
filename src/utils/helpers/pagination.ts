export const getPagination = (page?: number, size?: number) => {
    const limit: number = size ? size : 1;
    const offset: number = page ? (page - 1) * limit : 0;
    return { limit, offset };
};
