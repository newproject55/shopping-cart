import { IsNumber, IsOptional, Min } from "class-validator";
import { Type } from 'class-transformer';

export class PaginationQueryDto {
    @IsOptional()
    @IsNumber()
    @Type(() => Number)
    @Min(1)
    page?: number;
  
    @IsOptional()
    @IsNumber()
    @Min(1)
    @Type(() => Number)
    size?: number;
}