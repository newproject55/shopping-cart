import { IsNumber, Min, IsOptional, IsString, IsEnum } from 'class-validator';
import { OrderQueryParams } from '../enum/order-query-param.enum';
import { PaginationQueryDto } from './pagination-query.dto';

export class PaginationParamsDto extends PaginationQueryDto {
  @IsOptional()
  @IsNumber()
  @Min(1)
  startId?: number;

  @IsOptional()
  @IsEnum(OrderQueryParams)
  order?: OrderQueryParams;

  @IsOptional()
  @IsString()
  sortField?: string;

  @IsOptional()
  @IsString()
  keyWord?: string;
}
