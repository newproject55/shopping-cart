import {
  Injectable,
  HttpException,
  NotFoundException,
  BadRequestException,
  HttpStatus
} from "@nestjs/common";
import { FindManyOptions, Like, Repository } from "typeorm";
import users from "./entity/users.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { CreateUserDto } from "./dto/create-user.dto";
import { UpdateUserDto } from "./dto/update-user.dto";
import { encodePassword } from "src/utils/bcrypt/encodePassword";
import { PostgresErrorCode } from "src/utils/enum/postgres-error-code.enum";
import { CloudinaryService } from "src/cloudinary/cloudinary.service";
import { getPagination } from "src/utils/helpers/pagination";
import { ChangePasswordDto } from "./dto/change-password.dto";
import { comparePassword } from "src/utils/bcrypt/comparePassword";
import { OrderQueryParams } from "src/utils/enum/order-query-param.enum";
import { removeVietnameseAccent } from "src/utils/helpers/charactor";

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(users)
    private usersRepository: Repository<users>,
    private readonly cloudinaryService: CloudinaryService
  ) {}

  async getUserByEmail(email: string) {
    try {
      const user = await this.usersRepository.findOne({ where: { email } });
      if (!user) throw new NotFoundException("Email not found");
      return user;
    } catch (error) {
      throw new HttpException(error.message, error.status);
    }
  }

  async getUserByPhoneNumber(phone_number: string) {
    try {
      const user = await this.usersRepository.findOne({ where: { phone_number } });
      if (!user) throw new NotFoundException("Email not found");
      return user;
    } catch (error) {
      throw new HttpException(error.message, error.status);
    }
  }

  async createUser(createUserDto: CreateUserDto) {
    try {
      const hashPassword = await encodePassword(createUserDto.password);
      createUserDto.password = hashPassword;
      const newUser = this.usersRepository.create(createUserDto);
      await this.usersRepository.save(newUser);
      return newUser;
    } catch (error) {
      await this.cloudinaryService.removeImage(createUserDto.avatar_name)
      if (error?.code === PostgresErrorCode.UniqueViolation) {
        throw new BadRequestException("Email or phone number already exist");
      }
      if (error?.code === PostgresErrorCode.NotNullViolation) {
        throw new BadRequestException("Some fields are not null or empty");
      }
      throw new HttpException(error.message, error.status);
    }
  }

  async getAllUser(page: number, size: number) {
    try {
      const { limit, offset } = getPagination(page, size);
      const [list, count] = await this.usersRepository
        .createQueryBuilder("users")
        .select([
          "users.id",
          "users.username",
          "users.email",
          "users.phone_number",
          "users.is_active",
          "users.avatar_link",
          "users.roles",
          "users.verify_email",
          "users.verify_contact"
        ])
        .orderBy({ "users.id": "ASC" })
        .skip(offset)
        .take(limit)
        .getManyAndCount();
      if (list.length === 0) {
        throw new BadRequestException("Can not found users!");
      }
      return { list, count, page, size };
    } catch (error) {
      throw new HttpException(error.message, error.status);
    }
  }

  async getUserById(id: number) {
    try {
      const user = await this.usersRepository.findOne({ where: { id } });
      if (!user) throw new NotFoundException("User not found");
      return user;
    } catch (error) {
      throw new HttpException(error.message, error.status);
    }
  }

  async updateUserById(id: number, data: UpdateUserDto) {
    try {
      if (data.email) {
        const user = await this.getUserByEmail(data.email);
        if (user.id !== id && user.email === data.email)
          throw new BadRequestException("Email already exists");
      }
      if (data.password) throw new BadRequestException("Can not update password");
      await this.usersRepository.update(id, data);
      const updateUser = await this.getUserById(id);
      await this.usersRepository.save(updateUser);
    } catch (error) {
      throw new HttpException(error.message, error.status);
    }
  }

  async updatePassword(id: number, input: ChangePasswordDto) {
    try {
      const user = await this.getUserById(id);
      const checkPassword = await comparePassword(input.old_password, user.password);
      if (!checkPassword) {
        throw new BadRequestException("Password incorrect!");
      }
      const hashNewPassword = await encodePassword(input.new_password);
      await this.usersRepository.update({ id }, {
        password: hashNewPassword
      });
      const updateUser = await this.getUserById(id);
      await this.usersRepository.save(updateUser);
    } catch (error) {
      throw new HttpException(error.message, error.status);
    }
  }

  async updateForgotPassword(id: number, newPasswordHash: string) {
    try {
      await this.usersRepository.update({ id }, {
        password: newPasswordHash
      });
      const updateUser = await this.getUserById(id);
      await this.usersRepository.save(updateUser);
    } catch (error) {
      throw new HttpException(error.message, error.status);
    }
  }

  async deleteUserById(id: number) {
    try {
      const user = await this.usersRepository.findOne({ where: { id } });
      if (!user) throw new NotFoundException("User not found");
      await this.usersRepository.remove(user);
    } catch (error) {
      throw new HttpException(error.message, error.status);
    }
  }

  async saveVerificationEmailToken(id: number, token: string) {
    try {
      await this.usersRepository.update({ id }, {
        verify_email_token: token
      });
      const updateUser = await this.getUserById(id);
      await this.usersRepository.save(updateUser);
    } catch (error) {
      throw new HttpException(error.message, error.status);
    }
  }

  async activeEmail(email: string) {
    try {
      await this.usersRepository.update({ email }, {
        verify_email: true
      });
      const updateUser = await this.getUserByEmail(email);
      await this.usersRepository.save(updateUser);
    } catch (error) {
      throw new HttpException(error.message, error.status);
    }
  }

  async activeContact(id: number) {
    try {
      await this.usersRepository.update({ id }, {
        verify_contact: true
      });
      const updateUser = await this.getUserById(id);
      await this.usersRepository.save(updateUser);
    } catch (error) {
      throw new HttpException(error.message, error.status);
    }
  }

  async saveForgotPasswordToken(id: number, token: string) {
    try {
      await this.usersRepository.update({ id }, {
        forgot_password_token: token
      })
      const updateUser = await this.getUserById(id);
      await this.usersRepository.save(updateUser);
    } catch (error) {
      throw new HttpException(error.message, error.status);
    }
  }

  async resetPassword(id: number, newPassword: string) {
    try {
      const hashPassword = await encodePassword(newPassword);
      await this.usersRepository.update({ id }, {
        password: hashPassword
      })
      const updateUser = await this.getUserById(id);
      await this.usersRepository.save(updateUser);
    } catch (error) {
      throw new HttpException(error.message, error.status);
    }
  }

  async saveVerifyContactToken(id: number, token: string) {
    try {
      await this.usersRepository.update({ id }, {
        verify_contact_token: token
      })
      const updateUser = await this.getUserById(id);
      await this.usersRepository.save(updateUser);
    } catch (error) {
      throw new HttpException(error.message, error.status);
    }
  }

  async searchUser(
    page?: number,
    size?: number,
    order?: OrderQueryParams,
    sortField?: string,
    keyWord?: string,
  ) {
    try {
      keyWord = keyWord ? removeVietnameseAccent(keyWord) : '';
      const where: FindManyOptions<users>['where'] = [
        { username: Like(`%${keyWord}%`) },
        { phone_number: Like(`%${keyWord}%`) },
        { email: Like(`%${keyWord}%`) },
      ];
      const { limit, offset } = getPagination(page, size);
      const data = await this.usersRepository.findAndCount({
        where,
        order: {
          [sortField || 'id']: order || 'ASC',
        },
        skip: offset,
        take: limit
      });
      return { data, page, limit };
    } catch (error) {
      throw new HttpException(
        error?.message || error,
        error?.status || HttpStatus.INTERNAL_SERVER_ERROR
      )
    }
  }

  async uploadAvatar(userId: number, avatarLink: string, avatarName: string) {
    try {
      const user = await this.getUserById(userId);
      const oldAvatarName = user.avatar_name;
      const updateUser = await this.usersRepository.update({ id: userId }, {
        avatar_link: avatarLink,
        avatar_name: avatarName
      })
      if (!updateUser) {
        throw new BadRequestException("Update avatar failed!");
      }
      await this.cloudinaryService.removeImage(oldAvatarName);
      return updateUser;
    } catch (error) {
      await this.cloudinaryService.removeImage(avatarName);
      throw new HttpException(
        error?.message || error,
        error?.status || HttpStatus.INTERNAL_SERVER_ERROR
      )
    }
  }
}
