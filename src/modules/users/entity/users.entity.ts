import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  OneToMany,
  JoinColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Index,
  BeforeInsert,
  BeforeUpdate,
} from 'typeorm';
import { Exclude } from 'class-transformer';
import tokens from '../../tokens/entity/tokens.entity';
import orders from 'src/modules/orders/entity/orders.entity';
import reviews from 'src/modules/reviews/entity/reviews.entity';
import products_of_cart from 'src/modules/products-of-cart/entity/products-of-cart.entity';
import { UserRole } from 'src/utils/enum/user-role.enum';
import { removeVietnameseAccent } from 'src/utils/helpers/charactor';

@Entity('users')
export default class users {
  @PrimaryGeneratedColumn()
  public id?: number;

  @Index('name_index')
  @Column()
  public username: string;

  @Index('search_name_index')
  @Column({ nullable: true })
  @Exclude()
  public username_search: string;

  @Column()
  @Exclude()
  public password: string;

  @Index('email_index')
  @Column({ unique: true })
  public email: string;

  @Column({ unique: true })
  public phone_number: string;

  @Column({ default: true })
  public is_active: boolean;

  @Column('text', { array: true, default: [UserRole.USER] })
  roles: UserRole[];

  @Column({
    default: 'https://cdn-icons-png.flaticon.com/512/1053/1053244.png?w=360'
  })
  public avatar_link: string;

  @Column({ default: false })
  public verify_contact: boolean;


  @Column({ nullable: true, default: null })
  public avatar_name: string;

  @Column({ default: false })
  public verify_email: boolean;

  @Column({ nullable: true, default: null })
  public verify_email_token: string;

  @Column({ nullable: true, default: null })
  public verify_contact_token: string;

  @Column({ nullable: true, default: null })
  public forgot_password_token: string;

  @OneToMany(() => orders, (orders) => orders.users, { cascade: true })
  @JoinColumn()
  orders: orders[]

  @OneToMany(() => products_of_cart, (products_of_cart) => products_of_cart.user, { cascade: true })
  @JoinColumn()
  products_of_cart: products_of_cart[]

  @OneToMany(() => reviews, (reviews) => reviews.users, { cascade: true })
  @JoinColumn()
  reviews: reviews[]

  @OneToMany(() => tokens, (tokens) => tokens.user, { cascade: true })
  @JoinColumn()
  tokens: tokens[]

  @CreateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  public created_at: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  public updated_at: Date;

  @BeforeInsert()
  async beforeInsert() {
    this.username_search = removeVietnameseAccent(this.username);
  }
  @BeforeUpdate()
  beforeUpdateProduct() {
    this.username_search = removeVietnameseAccent(this.username);
  }

}