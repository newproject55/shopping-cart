import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpStatus,
  Query,
  UseGuards,
  Req,
} from "@nestjs/common";
import { UsersService } from "./users.service";
import { CreateUserDto } from "./dto/create-user.dto";
import { ApiProperty, ApiTags } from "@nestjs/swagger";
import { UpdateUserDto } from "./dto/update-user.dto";
import { formatResponse } from "src/utils/helpers/formatResponse";
import { JwtAuthGuard } from "../auth/guard/jwt-auth.guard";
import { PaginationQueryDto } from "src/utils/dto/pagination-query.dto";
import RoleGuard from "../auth/guard/auth.guard";
import { UserRole } from "src/utils/enum/user-role.enum";
import { ChangePasswordDto } from "./dto/change-password.dto";
import { VerifyEmailGuard } from "../auth/guard/verify-email.guard";
import { UsersQueryParams } from "./dto/query-user.dto";
import { RequestWithUser } from "src/utils/interface/request-with-user.interface";
import { UpdateAvatarDto } from './dto/update-avatar-user.dto';

@ApiTags("User")
@Controller("account")
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @ApiProperty()
  @UseGuards(VerifyEmailGuard)
  @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN))
  @Post("add")
  async createUser(@Body() createUser: CreateUserDto) {
    try {
      const newUser = await this.usersService.createUser(createUser);
      return formatResponse(HttpStatus.OK, "Create user successfully!", newUser);
    } catch (error) {
      return formatResponse(error.status, error.message);
    }
  }

  @UseGuards(VerifyEmailGuard)
  @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN))
  @Get("list")
  async getAllUser(@Query() { page, size, order, sortField, keyWord }: UsersQueryParams) {
    try {
      // const allUsers = await this.usersService.getAllUser(page, size);
      const listSearchOrders = await this.usersService.searchUser(
        page,
        size,
        order,
        sortField,
        keyWord
    )
      return formatResponse(HttpStatus.OK, "Get users successfully!", listSearchOrders);
    } catch (error) {
      return formatResponse(error.status, error.message);
    }
  }

  @UseGuards(VerifyEmailGuard)
    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN))
    @Get()
    async findAll(
        @Query() { page, size, order, sortField, keyWord }: UsersQueryParams,) {
        try {
            const listSearchOrders = await this.usersService.searchUser(
                page,
                size,
                order,
                sortField,
                keyWord
            )
            return formatResponse(
                HttpStatus.OK,
                'Search users success!',
                listSearchOrders,
            );
        } catch (error) {
            return formatResponse(
                error.status||HttpStatus.INTERNAL_SERVER_ERROR,
                error?.message||error
            )
        }
    }

  @UseGuards(JwtAuthGuard,RoleGuard(UserRole.USER,UserRole.ADMIN))
  @Get("my-profile")
  async getMyProfile(@Req() request: any) {
    try {
      const profile = await this.usersService.getUserById(request?.user?.id);
      const { password, verify_email_token, verify_contact_token, forgot_password_token, ...other} = profile;
      return formatResponse(HttpStatus.OK, "Get profile success!", other);
    } catch (error) {
      return formatResponse(error.status, error.message);
    }
  }

  @UseGuards(VerifyEmailGuard)
  @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN))
  @Get(":id")
  async getSingleUser(@Param("id") id: number) {
    try {
      const user = await this.usersService.getUserById(id);
      const { password, verify_email_token, verify_contact_token, forgot_password_token, ...other} = user;
      return formatResponse(HttpStatus.OK, "Get user successfully!", other);
    } catch (error) {
      return formatResponse(error.status, error.message);
    }
  }

  @UseGuards(VerifyEmailGuard)
  @UseGuards(JwtAuthGuard, RoleGuard(UserRole.USER,UserRole.ADMIN))
  @Patch("update-profile")
  async updateProfile(@Req() request: any) {
    try {
      await this.usersService.updateUserById(request?.user?.id, request?.body);
      return formatResponse(HttpStatus.OK, "Update profile successfully!");
    } catch (error) {
      return formatResponse(error.status, error.message);
    }
  }

  @UseGuards(VerifyEmailGuard)
  @UseGuards(JwtAuthGuard, RoleGuard(UserRole.USER,UserRole.ADMIN))
  @Patch("update-password")
  async changePassword(@Req() request:any, @Body() changePassword: ChangePasswordDto) {
    try {
      await this.usersService.updatePassword(request?.user?.id, changePassword);
      return formatResponse(HttpStatus.OK, "Change password successfully!");
    } catch (error) {
      return formatResponse(error.status, error.message);
    }
  }

  @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN,UserRole.USER))
  @Patch("update-avatar")
  async updateAvatar(@Req() request: any, @Body() {avatar_link, avatar_name}:UpdateAvatarDto) {
    try {
      const userId = request.user.id;
      await this.usersService.uploadAvatar(userId,avatar_link,avatar_name);
      return formatResponse(HttpStatus.OK, "Update avatar successfully!");
    } catch (error) {
      return formatResponse(error.status, error.message);
    }
  }

  @UseGuards(VerifyEmailGuard)
  @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN))
  @Patch(":id")
  async updateUser(@Param("id") id: number, @Body() data: UpdateUserDto) {
    try {
      const updateUser = await this.usersService.updateUserById(id, data);
      return formatResponse(HttpStatus.OK, "Update user successfully!", updateUser);
    } catch (error) {
      return formatResponse(error.status, error.message);
    }
  }

  @UseGuards(VerifyEmailGuard)
  @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN))
  @Delete(":id")
  async deleteUser(@Param("id") id: number) {
    try {
      await this.usersService.deleteUserById(id);
      return formatResponse(HttpStatus.OK, "Delete user successfully!");
    } catch (error) {
      return formatResponse(error.status, error.message);
    }
  }
}
