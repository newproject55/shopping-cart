import { IsEnum } from 'class-validator';
import { PaginationParamsDto } from 'src/utils/dto/paginationParams.dto'; 
import { SortFieldUsersEnum } from 'src/utils/enum/sortFieldUsers.enum';


export class UsersQueryParams extends PaginationParamsDto {
  @IsEnum(SortFieldUsersEnum)
  sortField?: SortFieldUsersEnum | string;
}