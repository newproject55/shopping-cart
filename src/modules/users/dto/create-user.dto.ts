import {
  IsEmail,
  IsString,
  IsNotEmpty,
  MinLength,
  IsBoolean,
  IsOptional,
  IsEnum,
  IsArray,
  ArrayMinSize
} from "class-validator";
import { UserRole } from "src/utils/enum/user-role.enum";

export class CreateUserDto {
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @IsString()
  @IsNotEmpty()
  username: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(4)
  password: string;

  @IsString()
  @IsNotEmpty()
  phone_number: string;

  @IsBoolean()
  @IsOptional()
  is_active?: boolean;

  @IsArray()
  @IsOptional()
  @IsEnum(UserRole, { each: true })
  @ArrayMinSize(1)
  roles?: UserRole[];

  @IsString()
  @IsOptional()
  avatar_link?: string;

  @IsString()
  @IsOptional()
  avatar_name?: string;

  @IsBoolean()
  @IsOptional()
  verify_email?: boolean;

  @IsString()
  @IsOptional()
  verify_email_token?: string;

  @IsBoolean()
  @IsOptional()
  verify_contact?: boolean;

  @IsString()
  @IsOptional()
  verify_contact_token?: string;
}
