import { IsNotEmpty, IsString } from "class-validator";

export class UpdateAvatarDto {
    @IsString()
    @IsNotEmpty()
    avatar_link: string;

    @IsString()
    @IsNotEmpty()
    avatar_name: string;
}