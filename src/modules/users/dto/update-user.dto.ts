import {
  IsEmail,
  IsString,
  IsBoolean,
  IsOptional,
  IsEnum,
  IsArray
} from "class-validator";
import { UserRole } from "src/utils/enum/user-role.enum";

export class UpdateUserDto {
  @IsEmail()
  @IsOptional()
  email?: string;

  @IsString()
  @IsOptional()
  username?: string;

  @IsString()
  @IsOptional()
  password?: string;

  @IsString()
  @IsOptional()
  phone_number?: string;

  @IsBoolean()
  @IsOptional()
  is_active?: boolean;

  @IsArray()
  @IsOptional()
  @IsEnum(UserRole,{ each: true })
  roles?: UserRole[];

  @IsString()
  @IsOptional()
  avatar_link?: string;
}
