import {
    IsString,
    IsNotEmpty,
    IsNumber
  } from "class-validator";

export class CreateProductDto {
    @IsString()
    @IsNotEmpty()
    link_img: string;

    @IsString()
    @IsNotEmpty()
    img_name: string;

    @IsNumber()
    @IsNotEmpty()
    productId: number;
}