import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductsModule } from '../products/products.module';
import images_product from './entity/images-product.entity';
import { ImagesProductController } from './images-product.controller';
import { ImagesProductService } from './images-product.service';

@Module({
  imports: [TypeOrmModule.forFeature([images_product]), forwardRef(() => ProductsModule)],
  controllers: [ImagesProductController],
  providers: [ImagesProductService],
  exports: [ImagesProductService]
})
export class ImagesProductModule {}
