import { forwardRef, HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import products from '../products/entity/products.entity';
import { ProductsService } from '../products/products.service';
import images_product from './entity/images-product.entity';

@Injectable()
export class ImagesProductService {
    constructor(
        @InjectRepository(images_product)
        private imagesProductRepository: Repository<images_product>,
        @Inject(forwardRef(() => ProductsService))
        private productsService: ProductsService
    ){}

    async saveImage(url: string, img_name: string, product: any){
        try {
            const saveImage = {link_img: url, img_name: img_name,product: product};
            const createImage = this.imagesProductRepository.create(saveImage)
            await this.imagesProductRepository.save(createImage)
        } catch (error) {
            return new Error('Can not save image of product')
        } 
    }

    async deleteImageByProductId(id: number) {
        try {
            const findImage = await this.productsService.getProductById(id)
            let clone = JSON.parse(JSON.stringify(findImage))
            await this.imagesProductRepository.remove(clone.images_product)
        } catch (error) {
            throw new HttpException(
                'INTERNAL SERVER ERROR',
                HttpStatus.INTERNAL_SERVER_ERROR
            )
        }
    }
}
