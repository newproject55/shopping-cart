import products from 'src/modules/products/entity/products.entity';
import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne,
} from 'typeorm';

@Entity('images_product')
export default class images_product {
    @PrimaryGeneratedColumn()
    public id?: number;

    @Column()
    public link_img : string;

    @Column()
    public img_name : string;
 
    @ManyToOne(() => products , (product) => product.id, { onDelete: 'CASCADE' })
    public product: products;

    @CreateDateColumn({
      type: 'timestamp',
      default: () => 'CURRENT_TIMESTAMP(6)',
    })
    public created_at: Date;
  
    @UpdateDateColumn({
      type: 'timestamp',
      default: () => 'CURRENT_TIMESTAMP(6)',
      onUpdate: 'CURRENT_TIMESTAMP(6)',
    })
    public updated_at: Date;
}