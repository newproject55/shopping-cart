import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { JwtModule } from "@nestjs/jwt";
import { UsersModule } from "../users/users.module";
import { SmsService } from "./sms.service";

@Module({
    imports: [
        ConfigModule,
        JwtModule,
        UsersModule,
    ],
    providers: [SmsService],
    exports: [SmsService],
})
export class SmsModule {}