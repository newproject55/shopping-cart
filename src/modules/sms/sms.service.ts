import { Twilio } from "twilio";
import { ConfigService } from '@nestjs/config';
import { Injectable } from "@nestjs/common";
import { UsersService } from "../users/users.service";
import { randomNumberCode } from "src/utils/helpers/random-code-verify";
import { VerificationTokenPayload } from "src/utils/interface/verification-token-payload.interface";
import { TokensService } from "../tokens/tokens.service";

@Injectable()
export class SmsService {
    private twilioClient: Twilio;

    constructor(
        private readonly configService: ConfigService
    ) {
        const accountSid = this.configService.get('TWILIO_ACCOUNT_SID');
        const authToken = this.configService.get('TWILIO_AUTH_TOKEN');
        this.twilioClient = new Twilio(accountSid, authToken);
    }

    async initialSmsPhoneNumberVerification(phoneNumber: string) {
        /* cần upgrade tài khoản twilio mới sử dụng đc các API của Twilio*/
        /*
         const serviceSid = this.configService.get('TWILIO_VERIFICATION_SERVICE_SID');
         return this.twilioClient.verify.services(serviceSid).verifications.create({ to: '+84969044253', channel: 'sms'})
         return this.twilioClient.validationRequests
         .create({friendlyName: 'My customer phone number', phoneNumber: '+84354524422'})
         .then(validation_request => console.log(validation_request.friendlyName));
        */

        const senderPhoneNumber : string = this.configService.get('TWILIO_SENDER_PHONE_NUMBER');
        const idCountry : string = this.configService.get('TWILIO_ID_COUNTRY');  /* Id Viet Nam = +84*/
        const receiverPhoneNumber = idCountry+phoneNumber;
        const code = randomNumberCode(6);
        const message = `Shopping-cart verification code: ${code}`
        await this.twilioClient.messages
            .create({
                body: message,
                from: senderPhoneNumber,
                to: receiverPhoneNumber,
            });
        return code;
    }
}