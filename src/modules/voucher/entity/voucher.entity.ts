import { number } from '@hapi/joi';
import vouchers_of_order from 'src/modules/vouchers-of-order/entity/vouchers-of-order.entity';
import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    UpdateDateColumn,
    OneToMany,
    JoinColumn
} from 'typeorm';

@Entity('voucher')
export default class voucher {
    @PrimaryGeneratedColumn()
    public id?: number;
  
    @Column({ unique: true })
    public content: string;

    @Column({type: "float"})
    public value : number;

    @Column()
    public exprise_time : Date;

    @Column()
    public quantity : number;

    @Column({default: 0})
    public used: number

    @OneToMany(() => vouchers_of_order, (vouchers_of_order) => vouchers_of_order.vouchers, {cascade: true})
    @JoinColumn()
    vouchers_of_order: vouchers_of_order[]

    @CreateDateColumn({
      type: 'timestamp',
      default: () => 'CURRENT_TIMESTAMP(6)',
    })
    public created_at: Date;
  
    @UpdateDateColumn({
      type: 'timestamp',
      default: () => 'CURRENT_TIMESTAMP(6)',
      onUpdate: 'CURRENT_TIMESTAMP(6)',
    })
    public updated_at: Date;
}