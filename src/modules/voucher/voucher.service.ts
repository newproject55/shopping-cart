import { BadRequestException, HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PostgresErrorCode } from 'src/utils/enum/postgres-error-code.enum';
import { getPagination } from 'src/utils/helpers/pagination';
import { Repository } from 'typeorm';
import { ApplyVoucherDto } from './dto/apply-voucher.dto';
import { CreateVoucherDto } from './dto/create-voucher.dto';
import { UpdateVoucherDto } from './dto/update-voucher.dto';
import voucher from './entity/voucher.entity';

@Injectable()
export class VoucherService {
    constructor(
        @InjectRepository(voucher)
        private readonly voucherRepository: Repository<voucher>
    ){}

    async create(data: CreateVoucherDto){
        try {
            const exp_time = new Date(data.exprise_time)
            data.exprise_time = exp_time
            const newVoucher = this.voucherRepository.create(data)
            await this.voucherRepository.save(newVoucher)
            return newVoucher
        } catch (error) {
            if (error?.code === PostgresErrorCode.UniqueViolation) {
                throw new BadRequestException("Content already exist");
              }
            throw new HttpException(
                error.message,
                error.status
            )
        } 
    }

    async getAllVoucher( page: number, size: number ){
        try {
            const { limit, offset } = getPagination(page, size);
            const [list, count] = await this.voucherRepository.findAndCount({
                order: {['id']: 'ASC'},
                skip: offset,
                take: limit
            })
            if (list.length === 0) {
                throw new BadRequestException("Not found voucher");
            }
            return { list, count, page, size };
        } catch (error) {
            throw new HttpException(
                error.message,
                error.status
            )
        }
    }

    async getVoucherById(id: number){
        try {
            const findVoucher = await this.voucherRepository.findOne({
                where: {id}
            })
            if(!findVoucher) throw new BadRequestException('Not found voucher')
            return findVoucher
        } catch (error) {
            throw new HttpException(
                error.message,
                error.status
            )
        } 
    }

    async update(id: number, data: UpdateVoucherDto){
        try {
            if(data?.exprise_time){
                const exp_time = new Date(data.exprise_time)
                data.exprise_time = exp_time
            }
            await this.voucherRepository.update(id,data)
        } catch (error) {
            if (error?.code === PostgresErrorCode.UniqueViolation) {
                throw new BadRequestException("Content already exist");
              }
            throw new HttpException(
                error.message,
                error.status
            )
        } 
    }

    async applyVoucher(data: ApplyVoucherDto){
        try {
            const amount = await Promise.all(data.content.map( async (voucher) => {
                return new Promise( async (resolve, reject) => {
                    const findVoucher = await this.voucherRepository.findOne({
                        where: {content: voucher}
                    })
                    if(!voucher) reject('Not found voucher'+ voucher)
                    if(findVoucher.used>=findVoucher.quantity)
                    reject('Voucher has been used up')
                    const time = new Date()
                    if(time > findVoucher.exprise_time) reject('Expired time voucher')
                    const money_reduct = data.total*findVoucher.value/100
                    resolve(money_reduct)
                })
            }))
            amount.forEach((element) => {
                const money = Number(element)
                data.total -= money
            })
            return data.total
        } catch (error) {
            throw new HttpException(
                error.message||error,
                error.status||HttpStatus.BAD_REQUEST
            )
        }
    }

    async delete(id: number){
        try {
            const findVoucher = await this.voucherRepository.findOne({
                where: {id}
            })
            if(!findVoucher) throw new BadRequestException('Not found voucher')
            await this.voucherRepository.remove(findVoucher)
        } catch (error) {
            throw new HttpException(
                error.message,
                error.status
            )
        } 
    }
}
