import { Type } from "class-transformer";
import {
    IsString,
    IsNotEmpty,
    IsOptional,
    IsNumber,
    IsArray,
    IsDate,
    Min,
    max,
    Max
  } from "class-validator";

export class CreateVoucherDto {

    @IsString()
    @IsNotEmpty()
    content: string;
  
    @IsNumber()
    @IsNotEmpty()
    @Min(0)
    @Max(99)
    value: number;
  
    @IsNumber()
    @IsNotEmpty()
    @Min(1)
    quantity: number;

    @IsDate()
    @IsNotEmpty()
    @Type(() => Date)
    exprise_time: Date;

}