import { Type } from "class-transformer";
import {
    IsString,
    IsNotEmpty,
    IsOptional,
    IsNumber,
    IsArray,
    IsDate,
    Min,
    max,
    Max,
    ArrayMinSize
  } from "class-validator";

export class ApplyVoucherDto {

    @IsArray()
    @IsNotEmpty()
    @ArrayMinSize(1)
    content: string[];
  
    @IsNumber()
    @IsNotEmpty()
    @Min(0)
    total: number;

}