import { Type } from "class-transformer";
import {
    IsString,
    IsNotEmpty,
    IsOptional,
    IsNumber,
    IsArray,
    IsDate,
    Min,
    Max
  } from "class-validator";

export class UpdateVoucherDto {

    @IsString()
    @IsOptional()
    content: string;;
  
    @IsNumber()
    @IsOptional()
    @Min(0)
    @Max(99)
    value: number;

    @IsNumber()
    @IsOptional()
    @Min(1)
    quantity: number;

    @IsNumber()
    @IsOptional()
    @Min(0)
    used: number;
  
    @IsDate()
    @IsOptional()
    @Type(()=> Date)
    exprise_time: Date;

}