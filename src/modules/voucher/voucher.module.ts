import { Module } from '@nestjs/common';
import { VoucherService } from './voucher.service';
import { VoucherController } from './voucher.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import voucher from './entity/voucher.entity';

@Module({
  imports: [TypeOrmModule.forFeature([voucher])],
  providers: [VoucherService],
  controllers: [VoucherController],
  exports: [VoucherService]
})
export class VoucherModule {}
