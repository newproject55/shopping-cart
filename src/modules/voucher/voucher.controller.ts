import { Body, Controller, Delete, Get, HttpStatus, Param, Patch, Post, Query, UseGuards } from '@nestjs/common';
import { PaginationQueryDto } from 'src/utils/dto/pagination-query.dto';
import { UserRole } from 'src/utils/enum/user-role.enum';
import { formatResponse } from 'src/utils/helpers/formatResponse';
import RoleGuard from '../auth/guard/auth.guard';
import { JwtAuthGuard } from '../auth/guard/jwt-auth.guard';
import { ApplyVoucherDto } from './dto/apply-voucher.dto';
import { CreateVoucherDto } from './dto/create-voucher.dto';
import { UpdateVoucherDto } from './dto/update-voucher.dto';
import { VoucherService } from './voucher.service';

@Controller('voucher')
export class VoucherController {
    constructor(
        private readonly voucherService: VoucherService
    ){}

    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN))
    @Post('add')
    async addVoucher(@Body() data: CreateVoucherDto){
        try {
            const result = await this.voucherService.create(data)
            return formatResponse(
                HttpStatus.OK,
                'Create Voucher Successfully!',
                result
            )
        } catch (error) {
            return formatResponse(
                error.status||HttpStatus.INTERNAL_SERVER_ERROR,
                error?.message||error
            )
        }
    }

    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN, UserRole.USER))
    @Post('apply-coupon')
    async applyCoupon(@Body() voucher: ApplyVoucherDto){
        try {
            const result = await this.voucherService.applyVoucher(voucher)
            return formatResponse(
                HttpStatus.OK,
                'Apply voucher successfully',
                result
            )
        } catch (error) {
            return formatResponse(
                error.status||HttpStatus.INTERNAL_SERVER_ERROR,
                error?.message||error
            )
        }
    }

    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN, UserRole.USER))
    @Get('list')
    async getAllVoucher(@Query() { page, size }: PaginationQueryDto){
        try {
            const allVouchers = await this.voucherService.getAllVoucher(page, size);
            return formatResponse( 
                HttpStatus.OK, 
                "Get voucher successfully!", 
                allVouchers 
            );
        } catch (error) {
            return formatResponse(
                error.status||HttpStatus.INTERNAL_SERVER_ERROR,
                error?.message||error
            )
        }
    }

    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN))
    @Get(':id')
    async getVoucherById(@Param('id') id: number){
        try {
            const voucher = await this.voucherService.getVoucherById(id)
            return formatResponse(
                HttpStatus.OK,
                'Update Voucher Successfully!',
                voucher
            )
        } catch (error) {
            return formatResponse(
                error.status||HttpStatus.INTERNAL_SERVER_ERROR,
                error?.message||error
            )
        }
    }

    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN))
    @Patch('update/:id')
    async updateVoucher(@Param('id') id: number, @Body() data: UpdateVoucherDto){
        try {
            await this.voucherService.update(id,data)
            return formatResponse(
                HttpStatus.OK,
                'Update Voucher Successfully!'
            )
        } catch (error) {
            return formatResponse(
                error.status||HttpStatus.INTERNAL_SERVER_ERROR,
                error?.message||error
            )
        }
    }

    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN))
    @Delete('delete/:id')
    async deleteVoucher(@Param('id') id: number){
        try {
            await this.voucherService.delete(id)
            return formatResponse(
                HttpStatus.OK,
                'Delete Voucher Successfully!'
            )
        } catch (error) {
            return formatResponse(
                error.status||HttpStatus.INTERNAL_SERVER_ERROR,
                error?.message||error
            )
        }
    }
    
}
