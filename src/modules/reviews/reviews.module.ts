import { Module } from '@nestjs/common';
import { ReviewsController } from './reviews.controller';
import { ReviewsService } from './reviews.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import reviews from './entity/reviews.entity';
import products_of_order from '../products-of-order/entity/products-of-order.entity';
import { UsersModule } from 'src/modules/users/users.module';
import { ProductsModule } from '../products/products.module';
import { OrdersModule } from '../orders/orders.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([reviews, products_of_order,products_of_order]),
    UsersModule,
    ProductsModule,
    OrdersModule
  ],
  controllers: [ReviewsController],
  providers: [ReviewsService],
  exports: [ReviewsService]
})
export class ReviewsModule { }
