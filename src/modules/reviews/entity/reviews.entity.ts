import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne
} from 'typeorm';
import users from 'src/modules/users/entity/users.entity';
import products from 'src/modules/products/entity/products.entity';

@Entity('reviews')
export default class reviews {
  @PrimaryGeneratedColumn()
  public id?: number;

  @Column()
  public content: string;

  @Column()
  public rate: number;

  @ManyToOne(() => users, (users) => users.id, { onDelete: 'CASCADE' })
  public users: users;

  @ManyToOne(() => products, products => products.id, { onDelete: 'CASCADE' })
  public products: products;

  @CreateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  public created_at: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  public updated_at: Date;
}