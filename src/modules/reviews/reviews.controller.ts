import { Body, Controller, Delete, Get, HttpStatus, Param, Patch, Post, Query, Req, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from '../auth/guard/jwt-auth.guard';
import { CreateReviewDto } from './dto/create-review.dto';
import { formatResponse } from 'src/utils/helpers/formatResponse';
import { ReviewsService } from './reviews.service';
import RoleGuard from '../auth/guard/auth.guard';
import { UserRole } from 'src/utils/enum/user-role.enum';
import { PaginationQueryDto } from 'src/utils/dto/pagination-query.dto';
import { UpdateReviewDto } from './dto/update-review.dto';
import { RequestWithUser } from 'src/utils/interface/request-with-user.interface';

@Controller('reviews')
export class ReviewsController {
    constructor(private readonly reviewsService: ReviewsService) {}

    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.USER, UserRole.ADMIN))
    @Post("add")
    async createReview(@Req() request: RequestWithUser, @Body() createReview: CreateReviewDto) {
        try {
            const userId = request.user.id;
            const newReview = await this.reviewsService.createReview(userId, createReview);
            return formatResponse(HttpStatus.OK, "Create user successfully!", newReview);
        } catch (error) {
            return formatResponse(error.status, error.message);
        }
    }

    @Get("list/:productId")
    async getAllReview(@Param("productId") productId:number, @Query() { page, size }: PaginationQueryDto) {
        try {
            const allReview = await this.reviewsService.getAllReview(productId,page, size)
            return formatResponse(HttpStatus.OK, "Get list review successfully!", allReview);
        } catch (error) {
            return formatResponse(error.status, error.message);
        }
    }

    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.USER, UserRole.ADMIN))
    @Patch("update-review")
    async updateReview(@Req() request: RequestWithUser, @Body() updateReview: UpdateReviewDto) {
        try {
            const userId = request.user.id;
            const update = await this.reviewsService.updateReview(userId, updateReview);
            return formatResponse(HttpStatus.OK, "Update review successfully!", update);
        } catch (error) {
            return formatResponse(error.status, error.message);
        }
    }

    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.USER, UserRole.ADMIN))
    @Delete("remove-review")
    async deleteReview(@Req() request: RequestWithUser, @Body() updateReview: UpdateReviewDto) {
        try {
            const userId = request.user.id;
            await this.reviewsService.deleteReview(userId, updateReview);
            return formatResponse(HttpStatus.OK, "Delete review successfully!");
        } catch (error) {
            return formatResponse(error.status, error.message);
        }
    }
}
