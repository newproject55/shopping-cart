import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';

export class CreateReviewDto {
    @IsNotEmpty()
    @IsNumber()
    product_id: number 

    @IsOptional()
    @IsString()
    content?: string

    @IsOptional()
    @IsNumber()
    rate?: number
}