import { BadRequestException, HttpException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DataSource, Repository } from 'typeorm';
import reviews from './entity/reviews.entity';
import { CreateReviewDto } from './dto/create-review.dto';
import { UsersService } from '../users/users.service';
import { ProductsService } from '../products/products.service';
import { getPagination } from 'src/utils/helpers/pagination';
import { UpdateReviewDto } from './dto/update-review.dto';
import { OrdersService } from '../orders/orders.service';
import products_of_order from '../products-of-order/entity/products-of-order.entity';

@Injectable()
export class ReviewsService {
    constructor(
        @InjectRepository(reviews)
        private reviewsRepository: Repository<reviews>,
        @InjectRepository(products_of_order)
        private productsOfOrderRepository: Repository<products_of_order>,
        private readonly usersService: UsersService,
        private readonly ordersService: OrdersService,
        private readonly productsService: ProductsService,
        private dataSource: DataSource
    ) {}

    private async findReview(userId: number, productId: number) {
        const review = await this.reviewsRepository.findOne({
            where: {
                users: { id: userId },
                products: { id: productId }
            }
        });
        return review;
    }

    async createReview(userId: number, createReview: CreateReviewDto) {
        try {
            const { product_id, ...other } = createReview;
            const user = await this.usersService.getUserById(userId);
            const findReview = await this.findReview(user.id, product_id);
            if (findReview) {
                throw new BadRequestException("Review already exist!");
            }
            // transaction
            const querryRunner = this.dataSource.createQueryRunner();
            await querryRunner.connect();
            await querryRunner.startTransaction();
            try {
                // create new review
                const completeOrder = await this.ordersService.getCompleteOrder(userId, product_id);
                const product = await this.productsService.getProductById(product_id);
                if (!product || completeOrder.length === 0) {
                    throw new NotFoundException("Not found product!");
                }
                const data = {
                    users: user,
                    products: product,
                    ...other
                }
                const newReview = this.reviewsRepository.create(data);
                await this.reviewsRepository.save(newReview);
                // update rating
                const currentRate = await this.updateRateOfProduct(product.id);
                await this.productsService.updateRatingProduct(product.id, currentRate);
                await querryRunner.commitTransaction();
                return {
                    user_id: user.id,
                    product_id: product.id,
                    ...other
                };
            } catch (error) {
                await querryRunner.rollbackTransaction();
            } finally {
                await querryRunner.release();
            }
        } catch (error) {
            throw new HttpException(error.message, error.status)
        }
    }

    async getAllReview(productId: number, page: number, size: number) {
        try {
            const { limit, offset } = getPagination(page, size);
            const [list, count] = await this.reviewsRepository.findAndCount({
                select: {
                    id: true,
                    content: true,
                    rate: true
                },
                where: {
                    products: { id: productId },
                },
                order: { ['id']: 'DESC' },
                skip: offset,
                take: limit
            })
            if (list.length === 0) {
                throw new BadRequestException("Can not found review!");
            }
            return { list, count, page, size };
        } catch (error) {
            throw new HttpException(error.message, error.status);
        }
    }

    async updateReview(userId: number, data: UpdateReviewDto) {
        try {
            const findReview = await this.findReview(userId, data.product_id);
            if (!findReview) {
                throw new NotFoundException("Not found reviews!");
            }
            await this.reviewsRepository.update(findReview.id, {
                content: data.content,
                rate: data.rate
            })
            const updateReview = await this.reviewsRepository.findOne({ where: { id: findReview.id } });
            if (!updateReview) {
                throw new BadRequestException("Update failed!");
            }
            await this.reviewsRepository.save(updateReview);
            await this.productsService.getProductById(data.product_id);
            return updateReview;
        } catch (error) {
            throw new HttpException(error.message, error.status);
        }
    }

    async deleteReview(userId: number, data: UpdateReviewDto) {
        try {
            const findReview = await this.findReview(userId, data.product_id);
            if (!findReview) {
                throw new NotFoundException("Not found reviews!");
            }
            await this.reviewsRepository.remove(findReview);
        } catch (error) {
            throw new HttpException(error.message, error.status);
        }
    }

    async getAllReviewByProductId(productId: number) {
        try {
            const reviews = await this.reviewsRepository.find({
                where: { products: { id: productId } }
            })
            return reviews;
        } catch (error) {
            throw new HttpException(error.message, error.status);
        }
    }

    async updateRateOfProduct(productId: number) {
        try {
            const reviews = await this.getAllReviewByProductId(productId);
            let arrayRate = [];
            reviews.forEach((v) => { arrayRate.push(v.rate) });
            let averageRate = arrayRate.reduce((a, b) => a + b, 0) / arrayRate.length;
            return Math.round((averageRate + Number.EPSILON) * 100) / 100;
        } catch (error) {
            throw new HttpException(error.message, error.status);
        }
    }
}
