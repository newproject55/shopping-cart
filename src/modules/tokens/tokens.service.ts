import tokens from './entity/tokens.entity';
import { Injectable, HttpException, NotFoundException, UnauthorizedException, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateTokensDto } from './dto/create-tokens.dto';
import { UsersService } from '../users/users.service';
import { JwtSign, TokenPayload } from 'src/utils/interface/jwt.interface';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { LogOutDto } from '../auth/dto/log-out.dto';
import { VerificationTokenPayload } from 'src/utils/interface/verification-token-payload.interface';

@Injectable()
export class TokensService {
  constructor(
    @InjectRepository(tokens)
    private tokensRepository: Repository<tokens>,
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
    private readonly usersService: UsersService,
  ) {}

  private generateRefreshToken(id: number): string {
    return this.jwtService.sign(
      { id },
      {
        secret: this.configService.get('JWT_REFRESH_SECRET'),
        expiresIn: this.configService.get('JWT_REFRESH_TOKEN_EXPIRATION_TIME'),
      },
    );
  }

  private generateAccessToken(payload: any): string {
    return this.jwtService.sign(
      { payload },
      {
        secret: this.configService.get('JWT_ACCESS_SECRET'),
        expiresIn: this.configService.get('JWT_ACCESS_TOKEN_EXPIRATION_TIME'),
      },
    );
  }

  private async getToken(access_token: string, client_id: string) {
    const tokenFound = await this.tokensRepository.findOne({ where: { access_token, client_id } });
    if (!tokenFound) {
      throw new NotFoundException('Token not found');
    }
    return tokenFound;
  }

  public decodeToken(token:string, jwtSecret:string) {
    if (!this.jwtService.verify(token, { secret: jwtSecret })) {
      throw new UnauthorizedException('Token expired!');
    }
    const payloadToken = this.jwtService.decode(token);
    if (!payloadToken) {
      throw new UnauthorizedException('Token expired!');
    }
    return payloadToken
  }

  public generateVerificationToken(payload: VerificationTokenPayload) {
    return this.jwtService.sign(
      { payload },
      {
        secret: this.configService.get('JWT_VERIFICATION_TOKEN_SECRET'),
        expiresIn: this.configService.get('JWT_VERIFICATION_TOKEN_EXPIRATION_TIME')
      });
  }

  public generateTokens(data: TokenPayload): JwtSign {
    const payload: TokenPayload = {
      id: data.id,
      email: data.email,
      roles: data.roles,
    };
    return {
      access_token: this.generateAccessToken(payload),
      refresh_token: this.generateRefreshToken(payload.id),
    };
  }

  public async saveToken(createTokenDto: CreateTokensDto) {
    try {
      const findUser = await this.usersService.getUserById(
        createTokenDto.userId,
      );
      const { client_id, ...data } = createTokenDto;
      const existToken = await this.tokensRepository.findOne({
        where: { client_id },
      });
      if (existToken) {
        await this.tokensRepository.remove(existToken);
      }
      const token = this.tokensRepository.create(createTokenDto);
      token.user = findUser;
      await this.tokensRepository.save(token);
    } catch (error) {
      throw new HttpException(error.message, error.status);
    }
  }

  public async deleteToken(logOutDto: LogOutDto) {
    try {
      const { access_token, client_id } = logOutDto
      const token = await this.getToken(access_token, client_id);
      await this.tokensRepository.remove(token);
    } catch (error) {
      throw new HttpException(error.message, error.status);
    }
  }

  public async generatNewTokens(refresh_token: string, client_id: string) {
    try {
      const token = await this.tokensRepository.findOne({ where: { refresh_token, client_id } });
      if (!token) {
        throw new NotFoundException('Token not found');
      }
      const payloadToken = this.decodeToken(token.refresh_token,this.configService.get("JWT_REFRESH_SECRET"));
      const userId = Object.values(payloadToken)[0];
      const user = await this.usersService.getUserById(userId);
      const newToken = this.generateTokens({
        id: user.id,
        email: user.email,
        roles: user.roles
      })
      return {
        token: newToken,
        id: user.id
      };
    } catch (error) {
      throw new HttpException(error.message, error.status);
    }
  }

  public async verifyForgotPasswordToken(token: string, code: string) {
    try {
      const payloadToken = this.decodeToken(token,this.configService.get('JWT_VERIFICATION_TOKEN_SECRET'));
      const payloadContent = Object.values(payloadToken)[0];
      const codePayload = Object.values(payloadContent)[0];
      if (!codePayload) {
        throw new BadRequestException("Can not found code!");
      }
      if (codePayload === code) {
        return true;
      }
    } catch (error) {
      throw new HttpException(error.message, error.status);
    }
  }
}
