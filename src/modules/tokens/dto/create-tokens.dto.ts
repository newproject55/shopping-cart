import {
    IsString,
    IsNotEmpty,
    IsOptional,
    IsNumber
  } from 'class-validator';
  
export class CreateTokensDto {

    @IsString()
    @IsNotEmpty()
    access_token: string;

    @IsString()
    @IsNotEmpty()
    client_id: string;

    @IsString()
    @IsNotEmpty()
    refresh_token: string;

    @IsNumber()
    @IsNotEmpty()
    userId: number;
}