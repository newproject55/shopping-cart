import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne
} from 'typeorm';

import Users from '../../users/entity/users.entity';

@Entity('tokens')
export default class tokens {
    @PrimaryGeneratedColumn()
    public id?: number;
  
    @Column()
    public client_id: string;

    @Column()
    public access_token : string;

    @Column()
    public refresh_token: string;
  
    @ManyToOne(() => Users , (user) => user.id, { onDelete: 'CASCADE' })
    public user: Users;

    @CreateDateColumn({
      type: 'timestamp',
      default: () => 'CURRENT_TIMESTAMP(6)',
    })
    public created_at: Date;
  
    @UpdateDateColumn({
      type: 'timestamp',
      default: () => 'CURRENT_TIMESTAMP(6)',
      onUpdate: 'CURRENT_TIMESTAMP(6)',
    })
    public updated_at: Date;
}