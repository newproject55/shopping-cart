import products_of_category from '../../products-of-category/entity/products-of-category.entity';
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm';

@Entity('category')
export default class category {
  @PrimaryGeneratedColumn()
  public id?: number;

  @Column({unique: true})
  public name: string;

  @OneToMany(() => products_of_category, productToCategory => productToCategory.category, { cascade: true})
  public categories: products_of_category[];

  @CreateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  public created_at: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  public updated_at: Date;
}
