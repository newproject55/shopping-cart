import { Body, Controller, Delete, Get, HttpStatus, Param, Patch, Post, Query, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { PaginationQueryDto } from 'src/utils/dto/pagination-query.dto';
import { UserRole } from 'src/utils/enum/user-role.enum';
import { formatResponse } from 'src/utils/helpers/formatResponse';
import RoleGuard from '../auth/guard/auth.guard';
import { JwtAuthGuard } from '../auth/guard/jwt-auth.guard';
import { CategoryService } from './category.service';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';

@ApiTags("Categories")
@Controller('categories')
export class CategoryController {
    constructor(private readonly categoryService: CategoryService) {}

    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN))
    @Post("add")
    async createCategory(@Body() name: CreateCategoryDto) {
        try {
            const newCategory = await this.categoryService.createCategory(name);
            return formatResponse(HttpStatus.OK, "Create user successfully!", newCategory);
        } catch (error) {
            return formatResponse(error.status, error.message);
        }
    }

    @Get("list")
    async getListCategories(@Query() { page, size }: PaginationQueryDto) {
        try {
            const allCategory = await this.categoryService.getAllCategories(page, size);
            return formatResponse(HttpStatus.OK, "Get users successfully!", allCategory);
        } catch (error) {
            return formatResponse(error.status, error.message);
        }
    }

    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN, UserRole.USER))
    @Get(":id")
    async getSingleCategory(@Param("id") id: number) {
        try {
            const category = await this.categoryService.getCategoryById(id);
            return formatResponse(HttpStatus.OK, "Get category successfully!", category);
        } catch (error) {
            return formatResponse(error.status, error.message);
        }
    }

    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN))
    @Patch(":id")
    async updateUser(@Param("id") id: number, @Body() data: UpdateCategoryDto) {
        try {
            const updateUser = await this.categoryService.updateCategoryById(id, data);
            return formatResponse(HttpStatus.OK, "Update category successfully!", updateUser);
        } catch (error) {
            return formatResponse(error.status, error.message);
        }
    }

    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN))
    @Delete(":id")
    async deleteUser(@Param("id") id: number) {
        try {
            await this.categoryService.deleteCategoryById(id);
            return formatResponse(HttpStatus.OK, "Delete category successfully!");
        } catch (error) {
            return formatResponse(error.status, error.message);
        }
    }
}
