import { BadRequestException, HttpException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getPagination } from 'src/utils/helpers/pagination';
import { Repository } from 'typeorm';
import { CreateCategoryDto } from './dto/create-category.dto';
import category from './entity/category.entity';
import { UpdateCategoryDto } from './dto/update-category.dto';

@Injectable()
export class CategoryService {
    constructor(
        @InjectRepository(category)
        private categoriesRepository: Repository<category>,
    ) {}

    async createCategory(name: CreateCategoryDto) {
        try {
            const newCategory = this.categoriesRepository.create(name);
            await this.categoriesRepository.save(newCategory);
            return newCategory;
        } catch (error) {
            throw new HttpException(error.message, error.status);
        }
    }

    async getAllCategories(page: number, size: number) {
        try {
            const { limit, offset } = getPagination(page, size);
            const [list, count] = await this.categoriesRepository
                .createQueryBuilder("category")
                .select([
                    "category.id",
                    "category.name"
                ])
                .orderBy({ "category.id": "ASC" })
                .skip(offset)
                .take(limit)
                .getManyAndCount();
            if (list.length === 0) {
                throw new BadRequestException("Can not found categories");
            }
            return { list, count, page, size };
        } catch (error) {
            throw new HttpException(error.message, error.status);
        }
    }

    async getCategoryById(id: number) {
        try {
            const category = await this.categoriesRepository.findOne({ where: { id } });
            if (!category) throw new NotFoundException("Not found category");
            return category;
        } catch (error) {
            throw new HttpException(error.message, error.status);
        }
    }

    async updateCategoryById(id: number, data: UpdateCategoryDto) {
        try {
            const category = await this.categoriesRepository.findOne({ where: { id } });
            if (!category) {
                throw new NotFoundException("Not found Category!")
            }
            await this.categoriesRepository.update(id, data);
            const updateCategory = await this.categoriesRepository.findOne({ where: { id } });
            if (!updateCategory)
                throw new BadRequestException("Update failed! User not found!");
            await this.categoriesRepository.save(updateCategory);
        } catch (error) {
            throw new HttpException(error.message, error.status);
        }
    }

    async deleteCategoryById(id: number) {
        try {
            const category = await this.categoriesRepository.findOne({ where: { id } });
            if (!category) throw new NotFoundException("Category not found");
            await this.categoriesRepository.remove(category);
          } catch (error) {
            throw new HttpException(error.message, error.status);
          }
    }
}
