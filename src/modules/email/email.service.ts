import { MailerService } from '@nestjs-modules/mailer';
import { BadRequestException, HttpException, Injectable, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../users/users.service';
import { CreateEmailDto } from './dto/create-email.dto';

@Injectable()
export class EmailService {
  constructor(
    private mailerService: MailerService,
    private readonly configService: ConfigService,
    private readonly jwtService: JwtService,
    private readonly usersService: UsersService
  ) {}
  
  async sendEmail(createMail: CreateEmailDto) {
    try {
      const { to, subject, text } = createMail;
      await this.mailerService.sendMail({
        to,
        from: this.configService.get('EMAIL_FROM'),
        subject,
        text,
      });
    } catch (error) {
      throw new BadRequestException('Something wrong when send email');
    }
  }

  async verifyEmail(email: string) {
    try {
      const user = await this.usersService.getUserByEmail(email);
      if (user.verify_email === true) {
        throw new BadRequestException("Email has been verification!");
      }
      await this.usersService.activeEmail(email);
    } catch (error) {
      throw new HttpException(error.message, error.status);
    }
  }

  async decodeVerifyEmailToken(token: string) {
    try {
      if (!this.jwtService.verify(token, { secret: this.configService.get('JWT_VERIFICATION_TOKEN_SECRET') })) {
        throw new UnauthorizedException('Refresh token expired');
      }
      const payloadToken = this.jwtService.decode(token);
      if (!payloadToken) {
        throw new UnauthorizedException('Refresh token expired');
      }
      const payloadContent = Object.values(payloadToken)[0];
      const email: any = Object.values(payloadContent)[0];
      if (!email) {
        throw new BadRequestException("Can not decode payload");
      }
      return email;
    } catch (error) {
      if (error?.name === 'TokenExpiredError') {
        throw new BadRequestException('Verify email token expired!');
      }
      throw new BadRequestException("Bad verify token");
    }
  }
}
