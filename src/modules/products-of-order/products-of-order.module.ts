import { Module } from '@nestjs/common';
import { ProductsOfOrderController } from './products-of-order.controller';
import { ProductsOfOrderService } from './products-of-order.service';

@Module({
  controllers: [ProductsOfOrderController],
  providers: [ProductsOfOrderService]
})
export class ProductsOfOrderModule {}
