import {
    IsString,
    IsNotEmpty,
    IsNumber,
    IsBoolean,
    IsArray,
    IsOptional,
    IsEnum,
    ArrayMinSize,
    IsPhoneNumber,
    IsEmail,
    Min,
    Max
  } from "class-validator";


export class CreateProductsOfOrderDto {
   
    @IsNumber()
    @IsNotEmpty()
    product_id: number;

    @IsString()
    @IsNotEmpty()
    name: string;

    @IsString()
    @IsNotEmpty()
    thumbnail_image: string;

    @IsString()
    @IsNotEmpty()
    brand: string;

    @IsString()
    @IsNotEmpty()
    description: string;

    @IsNumber()
    @IsNotEmpty()
    @Min(1)
    quantity: number;
    
    @IsNumber()
    @IsNotEmpty()
    @Min(0)
    price: number;

    @IsNumber()
    @IsNotEmpty()
    @Min(0)
    @Max(99)
    discount_percent: number;

    @IsNumber()
    @IsNotEmpty()
    @Min(0)
    total_price: number;

    @IsNumber()
    @IsNotEmpty()
    orderId: number;

}