import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne,
} from 'typeorm';
import orders from 'src/modules/orders/entity/orders.entity';
@Entity('products_of_order')
export default class products_of_order {
    @PrimaryGeneratedColumn()
    public id?: number;
  
    @Column()
    public product_id: number;

    @Column()
    public name: string;

    @Column()
    public thumbnail_image: string

    @Column()
    public brand: string;

    @Column()
    public description: string;

    @Column()
    public quantity : number;

    @Column({type: 'float'})
    discount_percent: number;

    @Column({type: 'float'})
    public price: number;

    @Column({type: 'float'})
    public total_price: number;

    @ManyToOne(() => orders , (order) => order.id, { onDelete: 'CASCADE' })
    public order: orders;
  
    @CreateDateColumn({
      type: 'timestamp',
      default: () => 'CURRENT_TIMESTAMP(6)',
    })
    public created_at: Date;
  
    @UpdateDateColumn({
      type: 'timestamp',
      default: () => 'CURRENT_TIMESTAMP(6)',
      onUpdate: 'CURRENT_TIMESTAMP(6)',
    })
    public updated_at: Date;
}