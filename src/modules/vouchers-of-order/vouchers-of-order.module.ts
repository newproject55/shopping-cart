import { Module } from '@nestjs/common';
import { VouchersOfOrderController } from './vouchers-of-order.controller';
import { VouchersOfOrderService } from './vouchers-of-order.service';

@Module({
  controllers: [VouchersOfOrderController],
  providers: [VouchersOfOrderService]
})
export class VouchersOfOrderModule {}
