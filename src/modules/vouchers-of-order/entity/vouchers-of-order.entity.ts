import { IsNotEmpty } from 'class-validator';
import orders from 'src/modules/orders/entity/orders.entity';
import voucher from 'src/modules/voucher/entity/voucher.entity';
import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
    ManyToOne
} from 'typeorm';

@Entity('vouchers_of_order')
export default class vouchers_of_order {
    @PrimaryGeneratedColumn()
    public id?: number;

    @Column()
    voucher_name: string;

    @Column({type: 'float'})
    value: number;

    @ManyToOne(() => voucher , (vouchers) => vouchers.content)
    public vouchers: voucher;

    @ManyToOne(() => orders , (orders) => orders.id, { onDelete: 'CASCADE' })
    public orders: orders;
}