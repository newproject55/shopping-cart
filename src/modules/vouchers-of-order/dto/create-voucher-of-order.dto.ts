import { Type } from "class-transformer";
import {
    IsString,
    IsNotEmpty,
    IsOptional,
    IsNumber,
    IsArray,
    IsDate,
    Min,
    max,
    Max
  } from "class-validator";

export class CreateVoucherOfOrderDto {

    @IsString()
    @IsNotEmpty()
    voucher_name: string;

    @IsNumber()
    @IsNotEmpty()
    value: number;

    @IsNumber()
    @IsNotEmpty()
    voucherId: number;
  
    @IsNumber()
    @IsNotEmpty()
    orderId: number;

}