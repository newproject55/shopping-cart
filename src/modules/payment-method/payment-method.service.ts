import { BadRequestException, HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getPagination } from 'src/utils/helpers/pagination';
import { Repository } from 'typeorm';
import { CreatePaymentMethodDto } from './dto/create-payment-method.dto';
import { UpdatePaymentMethodDto } from './dto/update-payment-method.dto';
import payment_method from './entity/payment-method.entity';

@Injectable()
export class PaymentMethodService {
    constructor(
        @InjectRepository(payment_method)
        private paymentMethodRepository: Repository<payment_method>
    ){}
    
    async create(name: CreatePaymentMethodDto) {
        try {
            const newPaymentMethod = this.paymentMethodRepository.create(name)
            await this.paymentMethodRepository.save(newPaymentMethod)
            return newPaymentMethod
        } catch (error) {
            throw new HttpException(
                error.message,
                error.status
            )
        }
    }

    async getAllMethods(page: number, size: number){
        try {
            const { limit, offset } = getPagination(page, size);
            const [list, count] = await this.paymentMethodRepository.findAndCount({
                order: {['id']: 'ASC'},
                skip: offset,
                take: limit
            })
            if (list.length === 0) {
                throw new BadRequestException("Not found payment method");
            }
            return { list, count, page, size };
        } catch (error) {
            throw new HttpException(
                error.message,
                error.status
            )
        }
    }

    async getMethodById(id: number){
        try {
            const findMethod = await this.paymentMethodRepository.findOne({
                where: { id }
            })
            if(!findMethod) throw new BadRequestException('Not found');
            return findMethod
        } catch (error) {
            throw new HttpException(
                error.message,
                error.status
            )
        }
    }

    async deletePaymentMethodById(id: number){
        try {
            const findMethod = await this.paymentMethodRepository.findOne({
                where: { id }
            })
            if(!findMethod) throw new BadRequestException('Not found');
            await this.paymentMethodRepository.remove(findMethod)
        } catch (error) {
            throw new HttpException(
                error.message,
                error.status
            )
        }
    }

    async update(id: number, data: UpdatePaymentMethodDto){
        try {
            await this.paymentMethodRepository.update(id,data)
        } catch (error) {
            throw new HttpException(
                error.message,
                error.status
            )
        }
    }
}
