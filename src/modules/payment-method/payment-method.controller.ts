import { BadRequestException, Body, Controller, Delete, Get, HttpStatus, Param, Patch, Post, Query, UseGuards } from '@nestjs/common';
import { PaginationQueryDto } from 'src/utils/dto/pagination-query.dto';
import { UserRole } from 'src/utils/enum/user-role.enum';
import { formatResponse } from 'src/utils/helpers/formatResponse';
import RoleGuard from '../auth/guard/auth.guard';
import { JwtAuthGuard } from '../auth/guard/jwt-auth.guard';
import { CreatePaymentMethodDto } from './dto/create-payment-method.dto';
import { UpdatePaymentMethodDto } from './dto/update-payment-method.dto';
import { PaymentMethodService } from './payment-method.service';

@Controller('payment')
export class PaymentMethodController {
    constructor(
        private paymentMethodService: PaymentMethodService
    ) {}

    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN))
    @Post('add')
    async addPaymentMethod(@Body() body: CreatePaymentMethodDto){
        try {
            const paymentMethod = await this.paymentMethodService.create(body)
            if(!paymentMethod) throw new BadRequestException('Can not create payment method')
            return formatResponse(
                HttpStatus.OK,
                'Add payment method successfully',
                paymentMethod
            )
        } catch (error) {
            return formatResponse(
                error.status||HttpStatus.INTERNAL_SERVER_ERROR,
                error?.message||error
            )
        }
    }

    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN,UserRole.USER))
    @Get('list')
    async getListPaymentMethods(@Query() { page, size }: PaginationQueryDto){
        try {
            const allMethods = await this.paymentMethodService.getAllMethods( page, size );
            return formatResponse(
                HttpStatus.OK,
                'Successfully',
                allMethods
            )
        } catch (error) {
            return formatResponse(
                error.status||HttpStatus.INTERNAL_SERVER_ERROR,
                error?.message||error
            )
        }
    }
    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN,UserRole.USER))
    @Get(':id')
    async getMethodById(@Param('id') id: number){
        try {
            const method = await this.paymentMethodService.getMethodById(id)
            return formatResponse(
                HttpStatus.OK,
                'Successfully',
                method
            )
        } catch (error) {
            return formatResponse(
                error.status||HttpStatus.INTERNAL_SERVER_ERROR,
                error?.message||error
            )
        }
    }

    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN))
    @Delete(':id')
    async deleteMethod(@Param('id') id: number){
        try {
            await this.paymentMethodService.deletePaymentMethodById(id)
            return formatResponse(
                HttpStatus.OK,
                'Delete successfully'
            )
        } catch (error) {
            return formatResponse(
                error.status||HttpStatus.INTERNAL_SERVER_ERROR,
                error?.message||error
            ) 
        }
    }

    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN))
    @Patch(':id')
    async update(@Param('id') id: number,@Body() data: UpdatePaymentMethodDto){
        try {
            await this.paymentMethodService.update(id,data)
            return formatResponse(
                HttpStatus.OK,
                'Update successfully'
            )
        } catch (error) {
            return formatResponse(
                error.status||HttpStatus.INTERNAL_SERVER_ERROR,
                error?.message||error
            ) 
        }
    }
}
