import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import payment_method from './entity/payment-method.entity';
import { PaymentMethodController } from './payment-method.controller';
import { PaymentMethodService } from './payment-method.service';

@Module({
  imports: [TypeOrmModule.forFeature([payment_method])],
  controllers: [PaymentMethodController],
  providers: [PaymentMethodService],
  exports: [PaymentMethodService]
})
export class PaymentMethodModule {}
