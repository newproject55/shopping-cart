import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  JoinColumn,
  OneToMany,
} from 'typeorm';
import orders from 'src/modules/orders/entity/orders.entity';

@Entity('payment_method')
export default class payment_method {
  @PrimaryGeneratedColumn()
  public id?: number;

  @Column()
  public name: string;

  @OneToMany(() => orders, (orders) => orders.id)
  @JoinColumn()
  public orders: orders[]

  @CreateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  public created_at: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  public updated_at: Date;
}