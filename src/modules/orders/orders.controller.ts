import { Body, Controller, Delete, Get, HttpStatus, Param, Patch, Post, Query, Req, UseGuards } from '@nestjs/common';
import { PaginationQueryDto } from 'src/utils/dto/pagination-query.dto';
import { OrderQueryParams } from 'src/utils/enum/order-query-param.enum';
import { UserRole } from 'src/utils/enum/user-role.enum';
import { formatResponse } from 'src/utils/helpers/formatResponse';
import { RequestWithUser } from 'src/utils/interface/request-with-user.interface';
import RoleGuard from '../auth/guard/auth.guard';
import { JwtAuthGuard } from '../auth/guard/jwt-auth.guard';
import { VerifyEmailGuard } from '../auth/guard/verify-email.guard';
import { CreateOrderDto } from './dto/create-order.dto';
import { OrdersQueryParams } from './dto/query-orders.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { OrdersService } from './orders.service';

@Controller('orders')
export class OrdersController {
    constructor(
        private readonly ordersService: OrdersService
    ) {}

    @UseGuards(VerifyEmailGuard)
    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN, UserRole.USER))
    @Post('add')
    async createOrder(@Req() request: RequestWithUser,@Body() data: CreateOrderDto) {
        try {
            const userId = request.user.id;
            const newOrder = await this.ordersService.create(userId,data)
            return formatResponse(
                HttpStatus.OK,
                'Add Order Successfully',
                newOrder
            )
        } catch (error) {
            return formatResponse(
                error.status || HttpStatus.INTERNAL_SERVER_ERROR,
                error?.message || error
            )
        }
    }

    @UseGuards(VerifyEmailGuard)
    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN,UserRole.USER))
    @Get('my-order')
    async myOrder(@Query() { page, size }: PaginationQueryDto, @Req() request: any) {
        try {
            const result = await this.ordersService.myOrder(request.user.id,page, size)
            return formatResponse(
                HttpStatus.OK,
                'Get list order successfully',
                result
            )
        } catch (error) {
            return formatResponse(
                error.status || HttpStatus.INTERNAL_SERVER_ERROR,
                error?.message || error
            )
        }
    }

    @UseGuards(VerifyEmailGuard)
    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN))
    @Get('list')
    async getListOrder(@Query() { page, size }: PaginationQueryDto) {
        try {
            const result = await this.ordersService.getList(page, size)
            return formatResponse(
                HttpStatus.OK,
                'Get list order successfully',
                result
            )
        } catch (error) {
            return formatResponse(
                error.status || HttpStatus.INTERNAL_SERVER_ERROR,
                error?.message || error
            )
        }
    }

    @UseGuards(VerifyEmailGuard)
    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN))
    @Get()
    async findAll(
        @Query() { page, size, order, sortField, keyWord }: OrdersQueryParams,
      ) {
        try {
            const listSearchOrders = await this.ordersService.searchOrder(
                page,
                size,
                order,
                sortField,
                keyWord
            )
            return formatResponse(
                HttpStatus.OK,
                'Search orders success!',
                listSearchOrders,
            );
        } catch (error) {
            return formatResponse(
                error.status||HttpStatus.INTERNAL_SERVER_ERROR,
                error?.message||error
            )
        }
    }

    @UseGuards(VerifyEmailGuard)
    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN))
    @Get(':id')
    async getOrderById(@Param('id') id: number) {
        try {
            const Order = await this.ordersService.getById(id)
            return formatResponse(
                HttpStatus.OK,
                'Get order successfully',
                Order
            )
        } catch (error) {
            return formatResponse(
                error.status || HttpStatus.INTERNAL_SERVER_ERROR,
                error?.message || error
            )
        }
    }

    @UseGuards(VerifyEmailGuard)
    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN))
    @Patch(':id')
    async updateOrder(@Param('id') id: number, @Body() data: UpdateOrderDto) {
        try {
            await this.ordersService.update(id, data)
            return formatResponse(
                HttpStatus.OK,
                'Update order successfully'
            )
        } catch (error) {
            return formatResponse(
                error.status || HttpStatus.INTERNAL_SERVER_ERROR,
                error?.message || error
            )
        }
    }


    @UseGuards(VerifyEmailGuard)
    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN))
    @Delete(':id')
    async deleteOrder(@Param('id') id: number) {
        try {
            await this.ordersService.detele(id)
            return formatResponse(
                HttpStatus.OK,
                'Delete order successfully'
            )
        } catch (error) {
            return formatResponse(
                error.status || HttpStatus.INTERNAL_SERVER_ERROR,
                error?.message || error
            )
        }
    }

}
