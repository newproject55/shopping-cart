import {
  IsString,
  IsNotEmpty,
  IsNumber,
  IsBoolean,
  IsArray,
  IsOptional,
  IsEnum,
  ArrayMinSize,
  IsPhoneNumber,
  IsEmail
} from "class-validator";
import { OrderStatus } from "src/utils/enum/order-status.enum";

export class CreateOrderDto {
  @IsBoolean()
  @IsNotEmpty()
  paided: boolean;

  @IsString()
  @IsNotEmpty()
  address: string;

  @IsOptional()
  @IsEnum(OrderStatus)
  status?: OrderStatus;

  @IsNumber()
  @IsNotEmpty()
  total_price: number;

  @IsPhoneNumber()
  @IsNotEmpty()
  contact: string;

  @IsEmail()
  @IsNotEmpty()
  email: string;

  @IsNumber()
  @IsNotEmpty()
  payment_methodId: number;

  @IsArray()
  @IsNotEmpty()
  products_of_order: object[];

  @IsArray()
  @IsNotEmpty()
  vouchers_of_order: string[];
}