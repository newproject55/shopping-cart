import { IsEnum } from 'class-validator';
import { PaginationParamsDto } from 'src/utils/dto/paginationParams.dto'; 
import { SortFieldOrdersEnum } from 'src/utils/enum/sortFieldOrders.enum';

export class OrdersQueryParams extends PaginationParamsDto {
  @IsEnum(SortFieldOrdersEnum)
  sortField?: SortFieldOrdersEnum | string;
}
