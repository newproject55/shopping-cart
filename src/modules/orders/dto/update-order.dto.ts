import { IsNotEmpty, IsBoolean, IsEnum } from "class-validator";
import { OrderStatus } from "src/utils/enum/order-status.enum";

export class UpdateOrderDto {
  @IsBoolean()
  @IsNotEmpty()
  paided: boolean;

  @IsNotEmpty()
  @IsEnum(OrderStatus)
  status: OrderStatus;
}