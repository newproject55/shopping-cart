import { Module } from '@nestjs/common';
import { OrdersService } from './orders.service';
import { OrdersController } from './orders.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import orders from './entity/orders.entity';
import users from '../users/entity/users.entity';
import payment_method from '../payment-method/entity/payment-method.entity';
import products_of_cart from '../products-of-cart/entity/products-of-cart.entity';
import vouchers_of_order from '../vouchers-of-order/entity/vouchers-of-order.entity';
import products_of_order from '../products-of-order/entity/products-of-order.entity';
import voucher from '../voucher/entity/voucher.entity';
import products from '../products/entity/products.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      orders, 
      users, 
      payment_method,
      products_of_order, 
      vouchers_of_order,
      products_of_cart, 
      voucher,
      products
    ])
  ],
  providers: [OrdersService],
  controllers: [OrdersController],
  exports: [OrdersService]
})
export class OrdersModule { }
