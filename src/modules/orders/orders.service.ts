import { BadRequestException, HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, Like, Repository } from 'typeorm';
import payment_method from '../payment-method/entity/payment-method.entity';
import products_of_cart from '../products-of-cart/entity/products-of-cart.entity';
import products_of_order from '../products-of-order/entity/products-of-order.entity';
import users from '../users/entity/users.entity';
import voucher from '../voucher/entity/voucher.entity';
import vouchers_of_order from '../vouchers-of-order/entity/vouchers-of-order.entity';
import { CreateOrderDto } from './dto/create-order.dto';
import orders from './entity/orders.entity';
import { voucherOfOrder } from 'src/utils/interface/saveVoucherOfOrder';
import { getPagination } from 'src/utils/helpers/pagination';
import { removeVietnameseAccent } from 'src/utils/helpers/charactor';
import { OrderQueryParams } from 'src/utils/enum/order-query-param.enum';
import { UpdateOrderDto } from './dto/update-order.dto';
import { OrderStatus } from 'src/utils/enum/order-status.enum';
import products from '../products/entity/products.entity';
import { array } from '@hapi/joi';

@Injectable()
export class OrdersService {
    constructor(
        @InjectRepository(orders)
        private ordersRepository: Repository<orders>,
        @InjectRepository(users)
        private usersRepository: Repository<users>,
        @InjectRepository(payment_method)
        private paymentMethodRepository: Repository<payment_method>,
        @InjectRepository(products_of_order)
        private productsOfOrderRepository: Repository<products_of_order>,
        @InjectRepository(vouchers_of_order)
        private vouchersOfOrderRepository: Repository<vouchers_of_order>,
        @InjectRepository(voucher)
        private voucherRepository: Repository<voucher>,
        @InjectRepository(products_of_cart)
        private cartRepository: Repository<products_of_cart>,
        @InjectRepository(products)
        private productsRepository: Repository<products>
    ) {}

    async create(userId: number, orders: CreateOrderDto) {
        try {
            const { products_of_order, vouchers_of_order, payment_methodId, ...orther } = orders
            const findUser = await this.usersRepository.findOne({
                where: { id: userId }
            })
            if (!findUser) throw new BadRequestException('Not found user')
            // find payment method in database

            const productsTotal = await Promise.all(products_of_order.map(async (element) => {
                return new Promise(async (resolve, reject) => {
                    let product = JSON.parse(JSON.stringify(element))
                    const findproduct = await this.productsRepository.findOne({
                        where: {id: product.product_id}
                    })
                    if(!findproduct) {
                        return reject('Not found product')
                    }
                    if(findproduct.stock_quantity < product.quantity) return reject( findproduct.name + ' : Not enough quantity' )
                    const total = findproduct.price*product.quantity*(100-findproduct.discount_percent)/100;
                    resolve(total)
                })
            }))
            var total = 0;
            productsTotal.forEach((productTotal: number) => {
                total += productTotal
            })
            const amount = await Promise.all(vouchers_of_order.map( async (voucher) => {
                return new Promise( async (resolve, reject) => {
                    const findVoucher = await this.voucherRepository.findOne({
                        where: {content: voucher}
                    })
                    if(!findVoucher) return reject('Not found voucher: '+ voucher)
                    if(findVoucher.used>=findVoucher.quantity)
                    return reject('Voucher has been used up')
                    const time = new Date()
                    if(time > findVoucher.exprise_time) reject( voucher +' : Expired time voucher')
                    const money_reduct = total*findVoucher.value/100
                    resolve(money_reduct)
                })
            }))
            amount.forEach((element) => {
                const money = Number(element)
                total -= money
            })
            console.log(total)
            if(total != orders.total_price) throw new BadRequestException('Conflict price, price is wrong');
            const findPayment = await this.paymentMethodRepository.findOne({
                where: { id: payment_methodId }
            })
            if (!findPayment) throw new BadRequestException('Not found payment method')
            const newOrder = {
                users: findUser,
                payment_method: findPayment,
                ...orther
            }
            // add element to table order
            const addOrder = this.ordersRepository.create(newOrder)
            await this.ordersRepository.save(addOrder)
            await Promise.all(products_of_order.map(async (element) => {
                return new Promise(async (resolve) => {
                    let product = JSON.parse(JSON.stringify(element))
                    const findproduct = await this.productsRepository.findOne({
                        where: {id: product.product_id}
                    })
                    findproduct.stock_quantity -= product.quantity
                    const updateQuantity = this.productsRepository.create(findproduct)
                    await this.productsRepository.update(product.product_id, updateQuantity)
                    const addProductToOrder = {
                        order: addOrder,
                        product_id: findproduct.id,
                        name: findproduct.name,
                        description: findproduct.description,
                        price: findproduct.price,
                        discount_percent: findproduct.discount_percent,
                        brand: findproduct.brand,
                        quantity: product.quantity,
                        thumbnail_image: findproduct.thumbnail_image,
                        total_price: findproduct.price*product.quantity
                    }
                    const creatProduct = this.productsOfOrderRepository.create(addProductToOrder)
                    await this.productsOfOrderRepository.save(creatProduct)
                    resolve('successfully')
                })
            }))
            // add element to table vouchers_of_order
            await Promise.all(vouchers_of_order.map(async (element) => {
                return new Promise(async (resolve) => {
                    const voucher_name = JSON.parse(JSON.stringify(element))
                    const findVoucher = await this.voucherRepository.findOne({
                        where: { content: voucher_name }
                    })
                    const voucher: voucherOfOrder = {
                        value: findVoucher.value,
                        voucher_name: findVoucher.content,
                        orders: addOrder,
                        vouchers: findVoucher
                    }
                    const voucherOfOrder = this.vouchersOfOrderRepository.create(voucher)
                    await this.vouchersOfOrderRepository.save(voucherOfOrder)
                    resolve('added')
                })
            }))
            // delete product in cart 
            const findCartByUserId = await this.cartRepository.find({
                where: { user: { id: userId } }
            })
            await this.cartRepository.remove(findCartByUserId)
            return addOrder
        } catch (error) {
            throw new HttpException(
                error.message||error, 
                error.status||HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    async getList(page: number, size: number) {
        try {
            const { limit, offset } = getPagination(page, size);
            const [list, count] = await this.ordersRepository.findAndCount({
                order: { ['id']: 'DESC' },
                skip: offset,
                take: limit,
                relations: { users: true },
                select: { users: { username: true, id: true } }
            })
            if (list.length === 0) {
                throw new BadRequestException("Not found order");
            }
            return { list, count, page, size };
        } catch (error) {
            throw new HttpException(error.message, error.status)
        }
    }

    async myOrder(userId: number, page: number, size: number) {
        try {
            const { limit, offset } = getPagination(page, size);
            const [list, count] = await this.ordersRepository.findAndCount({
                where: { users: { id: userId }},
                order: { ['id']: 'DESC' },
                skip: offset,
                take: limit,
                relations: { users: true },
                select: { users: { username: true, id: true } }
            })
            if (list.length === 0) {
                throw new BadRequestException("Not found order");
            }
            return { list, count, page, size };
        } catch (error) {
            throw new HttpException(error.message, error.status)
        }
    }

    async searchOrder(
        page?: number,
        size?: number,
        order?: OrderQueryParams,
        sortField?: string,
        keyWord?: string,
    ) {
        try {
            keyWord = keyWord ? removeVietnameseAccent(keyWord) : '';
            const where: FindManyOptions<orders>['where'] = [
                { address_search: Like(`%${keyWord}%`) },
                { contact: Like(`%${keyWord}%`) },
                { email: Like(`%${keyWord}%`) },
            ];
            const { limit, offset } = getPagination(page, size);
            const data = await this.ordersRepository.findAndCount({
                where,
                order: {
                    [sortField || 'id']: order || 'ASC',
                },
                skip: offset,
                take: limit
            });
            return { data, page, limit };
        } catch (error) {
            throw new HttpException(
                error?.message || error,
                error?.status || HttpStatus.INTERNAL_SERVER_ERROR
            )
        }

    }

    async getById(id: number) {
        try {
            const findOrder = await this.ordersRepository.findOne({
                where: { id: id },
                relations: ['products_of_order', 'vouchers_of_order', 'payment_method', 'users']
            })
            if (!findOrder) throw new BadRequestException('Not found order')
            const user = {
                id: findOrder.users.id,
                username: findOrder.users.username,
                phone_number: findOrder.users.phone_number,
                avatar_link: findOrder.users.avatar_link,
                email: findOrder.users.email,
            }
            const payment = {
                id: findOrder.payment_method.id,
                name: findOrder.payment_method.name
            }
            const Order = JSON.parse(JSON.stringify(findOrder))
            Order.users = user
            Order.payment_method = payment
            return Order
        } catch (error) {
            throw new HttpException(error.message, error.status)
        }
    }

    async update(id: number, data: UpdateOrderDto) {
        try {
            const updateOrder = this.ordersRepository.create(data)
            await this.ordersRepository.update(id, updateOrder)
        } catch (error) {
            throw new HttpException(error.message, error.status)
        }
    }

    async detele(id: number) {
        try {
            const findOrderById = await this.ordersRepository.find({
                where: { id: id }
            })
            await this.ordersRepository.remove(findOrderById)
        } catch (error) {
            throw new HttpException(error.message, error.status)
        }
    }
    
    async getCompleteOrder(userId: number, productId: number) {
        const order = await this.ordersRepository.find({
            where: {
                paided: true,
                status: OrderStatus.COMPLETED,
                users: {
                    id: userId
                },
                products_of_order: {
                    product_id: productId
                }
            },
            order: { id: 'ASC' }
        });
        return order;
    }
}
