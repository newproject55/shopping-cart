import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  OneToMany,
  JoinColumn,
  BeforeInsert,
  Index
} from 'typeorm';
import users from 'src/modules/users/entity/users.entity';
import vouchers_of_order from 'src/modules/vouchers-of-order/entity/vouchers-of-order.entity';
import { OrderStatus } from 'src/utils/enum/order-status.enum';
import payment_method from 'src/modules/payment-method/entity/payment-method.entity';
import products_of_order from 'src/modules/products-of-order/entity/products-of-order.entity';
import { Exclude } from 'class-transformer';
import { removeVietnameseAccent } from 'src/utils/helpers/charactor';

@Entity('orders')
export default class orders {
  @PrimaryGeneratedColumn()
  public id?: number;

  @Index('address_index')
  @Column()
  public address: string;

  @Index('search_address_index')
  @Column({ nullable: true })
  @Exclude()
  public address_search: string;

  @Column({ default: false })
  public paided: boolean;

  @Column({ default: OrderStatus.PROCESSING })
  status: OrderStatus;

  @Index('search_contact_index')
  @Column()
  public contact: string;

  @Index('search_email_index')
  @Column()
  public email: string;

  @Column({ type: 'float' })
  public total_price: number;

  @ManyToOne(() => users, (users) => users.id, { onDelete: 'CASCADE' })
  public users: users;

  @OneToMany(() => vouchers_of_order, (vouchers_of_order) => vouchers_of_order.orders)
  @JoinColumn()
  public vouchers_of_order: vouchers_of_order[]

  @OneToMany(() => products_of_order, (products_of_order) => products_of_order.order, { cascade: true })
  @JoinColumn()
  public products_of_order: products_of_order[]

  @ManyToOne(() => payment_method, (payment_method) => payment_method.id)
  public payment_method: payment_method;

  @CreateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  public created_at: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  public updated_at: Date;

  @BeforeInsert()
  async beforeInsert() {
    this.address_search = removeVietnameseAccent(this.address);
  }


}