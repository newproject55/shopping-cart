import { number } from '@hapi/joi';
import { BadRequestException, forwardRef, HttpException, HttpStatus, Inject, Injectable, Post } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getPagination } from 'src/utils/helpers/pagination';
import { Repository } from 'typeorm';
import { threadId } from 'worker_threads';
import { ProductsService } from '../products/products.service';
import { UsersService } from '../users/users.service';
import { AddProductToCartDto } from './dto/add-product.dto';
import { UpdateCartDto } from './dto/update-cart.dto';
import products_of_cart from './entity/products-of-cart.entity';

@Injectable()
export class ProductsOfCartService {
    constructor(
        @InjectRepository(products_of_cart)
        private productOfCartRepository: Repository<products_of_cart>,
        private usersService: UsersService,
        private productsService: ProductsService
    ) { }

    async addProduct(userId: number, data: AddProductToCartDto) {
        try {
            const findUser = await this.usersService.getUserById(userId)
            if (!findUser) throw new BadRequestException('not found user')
            const findProduct = await this.productsService.getProductById(data.productId)
            if (!findProduct) throw new BadRequestException('not found product')
            const chekcProductOfCart = await this.productOfCartRepository.findOne({
                relations: {
                    product: true,
                    user: true
                },
                where: {
                    product: { id: data.productId },
                    user: { id: userId }
                }
            })
            if (chekcProductOfCart) {
                throw new BadRequestException('Product already exists')
            }
            const newProductInCart = this.productOfCartRepository.create(data)
            newProductInCart.user = findUser
            newProductInCart.product = findProduct
            await this.productOfCartRepository.save(newProductInCart);
            return { user_id: userId, ...data }
        } catch (error) {
            throw new HttpException(error.message,error.status)
        }
    }

    async getAllProductInCart(userId: number, page: number, size: number) {
        try {
            const { limit, offset } = getPagination(page, size);
            const [list, count] = await this.productOfCartRepository.findAndCount({
                where: { user: { id: userId } },
                order: { ['id']: 'ASC' },
                skip: offset,
                take: limit,
                relations: ['product']
            })
            if (list.length === 0) {
                throw new BadRequestException("Not found product in cart");
            }
            return { list, count, page, size };
        } catch (error) {
            throw new HttpException(error.message,error.status)
        }
    }

    async cartTotals(userId: number) {
        try {
            const findAllProduct = await this.productOfCartRepository.find({
                where: { user: { id: userId } },
                relations: ['product']
            })
            const productTotals = await Promise.all(findAllProduct.map(async (element) => {
                return new Promise((resolve, reject) => {
                    const total = element.quantity * element.product.price * (100 - element.product.discount_percent) / 100
                    resolve(total)
                })
            }))
            let totalElements = JSON.parse(JSON.stringify(productTotals))
            var initialValue = 0;
            totalElements.forEach((total: number) => {
                initialValue += total
            })
            return initialValue
        } catch (error) {
            throw new HttpException(error.message,error.status)
        }
    }

    async deleteCart(id: number) {
        try {
            const products = await this.productOfCartRepository.find({
                where: { user: { id } }
            })
            if (products.length === 0) throw new BadRequestException('Cart is empty')
            await this.productOfCartRepository.remove(products)
        } catch (error) {
            throw new HttpException(error.message,error.status)
        }
    }

    async deleteProduct(id: number) {
        try {
            const findProductInCart = await this.productOfCartRepository.findOne({
                where: { id }
            })
            if (!findProductInCart) throw new BadRequestException('Not found product in cart')
            await this.productOfCartRepository.remove(findProductInCart)
        } catch (error) {
            throw new HttpException(error.message,error.status)
        }
    }

    async update(id: number, data: UpdateCartDto) {
        try {
            const findProduct = await this.productOfCartRepository.findOne({
                where: { id },
                relations: ['product']
            })
            if (!findProduct) throw new BadRequestException('Not found');
            const newUpdate = {
                price: findProduct.product.price,
                quantity: data.quantity,
                total_price: findProduct.product.price * data.quantity
            }
            await this.productOfCartRepository.update(id, newUpdate)
        } catch (error) {
            throw new HttpException(error.message,error.status)
        }
    }

}
