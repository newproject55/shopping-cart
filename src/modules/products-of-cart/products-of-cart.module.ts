import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductsModule } from '../products/products.module';
import { UsersModule } from '../users/users.module';
import products_of_cart from './entity/products-of-cart.entity';
import { ProductsOfCartController } from './products-of-cart.controller';
import { ProductsOfCartService } from './products-of-cart.service';

@Module({
  imports: [TypeOrmModule.forFeature([products_of_cart]), UsersModule, ProductsModule],
  controllers: [ProductsOfCartController],
  providers: [ProductsOfCartService],
  exports: [ProductsOfCartService]
})
export class ProductsOfCartModule {}
