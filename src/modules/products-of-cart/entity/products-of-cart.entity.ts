import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne
} from 'typeorm';
import users from 'src/modules/users/entity/users.entity';
import products from 'src/modules/products/entity/products.entity';

@Entity('products_of_cart')
export default class products_of_cart {
    @PrimaryGeneratedColumn()
    public id?: number;

    @Column()
    public quantity : number;

    @Column()
    public price: number;

    @Column()
    public total_price: number;

    @ManyToOne(() => users , (user) => user.id, { onDelete: 'CASCADE' })
    public user: users;

    @ManyToOne(() => products , (product) => product.id, { onDelete: 'CASCADE' })
    public product: products;
  
    @CreateDateColumn({
      type: 'timestamp',
      default: () => 'CURRENT_TIMESTAMP(6)',
    })
    public created_at: Date;
  
    @UpdateDateColumn({
      type: 'timestamp',
      default: () => 'CURRENT_TIMESTAMP(6)',
      onUpdate: 'CURRENT_TIMESTAMP(6)',
    })
    public updated_at: Date;
}