import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Patch, Post, Query, Req, UseGuards } from '@nestjs/common';
import { PaginationQueryDto } from 'src/utils/dto/pagination-query.dto';
import { UserRole } from 'src/utils/enum/user-role.enum';
import { formatResponse } from 'src/utils/helpers/formatResponse';
import { RequestWithUser } from 'src/utils/interface/request-with-user.interface';
import RoleGuard from '../auth/guard/auth.guard';
import { JwtAuthGuard } from '../auth/guard/jwt-auth.guard';
import { AddProductToCartDto } from './dto/add-product.dto';
import { UpdateCartDto } from './dto/update-cart.dto';
import { ProductsOfCartService } from './products-of-cart.service';
@Controller('cart')
export class ProductsOfCartController {
    constructor(
        private readonly productsOfCartService: ProductsOfCartService
    ){}

    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN,UserRole.USER))
    @Post('add')
    async addProductToCart(@Req() request: RequestWithUser,@Body() addProductToCart: AddProductToCartDto) {
        try {
            const userId = request.user.id;
            const result = await this.productsOfCartService.addProduct(userId,addProductToCart)
            return formatResponse(
                HttpStatus.OK,
                'Add product to cart successfully',
                result
            )
        } catch (error) {
            return formatResponse(
                error.status,
                error?.message||error
            )
        }
    }

    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN,UserRole.USER))
    @Get('list')
    async getProductOfCart(@Req() request:RequestWithUser, @Query() { page, size }: PaginationQueryDto){
        try {
            const userId = request.user.id;
            const allProducts = await this.productsOfCartService.getAllProductInCart(userId, page, size);
            return formatResponse( 
                HttpStatus.OK, 
                "Get products in cart successfully!", 
                allProducts 
            );
        } catch (error) {
            return formatResponse(
                error.status,
                error?.message||error
            )
        }
    }
    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN,UserRole.USER))
    @Get('cart-totals')
    async getCartTotals(@Req() request:RequestWithUser){
        try {
            const userId = request.user.id;
            const Subtotal = await this.productsOfCartService.cartTotals(userId)
            const data = {
                Subtotal,
                Shipping: 20,
                Total: Subtotal + 20
            }
            return formatResponse(
                HttpStatus.OK,
                'Get cart totals successfully',
                data
            )
        } catch (error) {
            return formatResponse(
                error.status,
                error?.message||error
            )
        }
    }
    

    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN,UserRole.USER))
    @Patch(':id')
    async updateCart(@Param('id') id: number,@Body() data: UpdateCartDto) {
        try {
            await this.productsOfCartService.update(id,data)
            return formatResponse(
                HttpStatus.OK,
                'Update successfully'
            )
        } catch (error) {
            return formatResponse(
                error.status,
                error?.message||error
            )
        }
    }

    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN,UserRole.USER))
    @Delete('delete')
    async deleteAllProductInCart(@Req() req: any) {
        try {
            await this.productsOfCartService.deleteCart(req.user.id)
            return formatResponse(
                HttpStatus.OK,
                'Delete cart successfully'
            )
        } catch (error) {
            return formatResponse(
                error.status||HttpStatus.INTERNAL_SERVER_ERROR,
                error?.message||error
            )
        }
    }


    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN,UserRole.USER))
    @Delete('delete/:id')
    async deleteProductById(@Param('id') id: number) {
        try {
            await this.productsOfCartService.deleteProduct(id)
            return formatResponse(
                HttpStatus.OK,
                'Delete successfully'
            )
        } catch (error) {
            return formatResponse(
                error.status||HttpStatus.INTERNAL_SERVER_ERROR,
                error?.message||error
            )
        }
    }

}
