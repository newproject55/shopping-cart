import category from 'src/modules/category/entity/category.entity';
import products from 'src/modules/products/entity/products.entity';
import {
    Entity,
    ManyToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('products_of_category')
export default class products_of_category {
    @PrimaryGeneratedColumn()
    public id?: number;
  
    @ManyToOne(() => products, (products) => products.id, { onDelete: 'CASCADE' })
    public products: products

    @ManyToOne(() => category, (category) => category.name, { onDelete: 'CASCADE' })
    public category: category
}