import { Module } from '@nestjs/common';
import { ProductsOfCategoryController } from './products-of-category.controller';
import { ProductsOfCategoryService } from './products-of-category.service';

@Module({
  controllers: [ProductsOfCategoryController],
  providers: [ProductsOfCategoryService]
})
export class ProductsOfCategoryModule {}
