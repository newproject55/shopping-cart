import { CanActivate, ExecutionContext, Type, mixin, InternalServerErrorException } from '@nestjs/common';
import { Observable } from 'rxjs';
import users from 'src/modules/users/entity/users.entity';
import { UserRole } from '../../../utils/enum/user-role.enum';

const RoleGuard = (...requiredRoles: UserRole[]) : Type<CanActivate> => {
  class RoleGuardMixin implements CanActivate {
    canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
      const request = context.switchToHttp().getRequest();
      const user: users = request.user;
      if (requiredRoles.length === 0 ) throw new InternalServerErrorException("Not Authorization!");
      return user.roles.some((roles: UserRole) => {
        return requiredRoles.includes(roles);
      });
    }
  }
  return mixin(RoleGuardMixin);
};
export default RoleGuard;
