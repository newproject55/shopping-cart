import { CanActivate, ExecutionContext, Injectable, UnauthorizedException } from "@nestjs/common";
import { Observable } from "rxjs";

@Injectable()
export class VerifyEmailGuard implements CanActivate {
    canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
        const request = context.switchToHttp().getRequest();
        if(request.user?.verify_email === false) {
            throw new UnauthorizedException('Verify your email first');
        }
        return true;
    }
}

export class VerifyContactGuard implements CanActivate {
    canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
        const request = context.switchToHttp().getRequest();
        if(request.user?.verify_contact === false) {
            throw new UnauthorizedException('Verify your contact first');
        }
        return true;
    }
}
