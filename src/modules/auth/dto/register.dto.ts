import { ApiProperty } from "@nestjs/swagger";
import {
  IsEmail,
  IsString,
  IsNotEmpty,
  MinLength,
  IsBoolean,
  IsOptional,
  IsEnum,
  IsArray,
  ArrayMinSize
} from "class-validator";
import { UserRole } from "src/utils/enum/user-role.enum";

export class RegisterDto {
  @ApiProperty()
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  username: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @MinLength(4)
  password: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  phone_number: string;

  @IsBoolean()
  @IsOptional()
  @ApiProperty()
  is_active?: boolean;

  @IsArray()
  @IsOptional()
  @IsEnum(UserRole, { each: true })
  @ArrayMinSize(1)
  roles?: UserRole[];

  @IsString()
  @IsOptional()
  @ApiProperty()
  avatar_link?: string;

  @IsString()
  @IsOptional()
  @ApiProperty()
  avatar_name?: string;

  @IsBoolean()
  @IsOptional()
  @ApiProperty()
  verify_email?: boolean;

  @IsString()
  @IsOptional()
  @ApiProperty()
  verify_email_token?: string;

  @IsBoolean()
  @IsOptional()
  @ApiProperty()
  verify_contact?: boolean;

  @IsString()
  @IsOptional()
  @ApiProperty()
  verify_contact_token?: string;
}
