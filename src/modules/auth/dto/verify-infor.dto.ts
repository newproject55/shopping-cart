import { IsNotEmpty, IsString } from "class-validator";

export class VerifyEmailDto {
    @IsString()
    @IsNotEmpty()
    verify_email_token: string;
}

export class VerifyContactDto {
    @IsString()
    @IsNotEmpty()
    verify_contact_token: string;
}

export class VerifyEmaiQuerylDto {
    @IsString()
    @IsNotEmpty()
    token: string
}