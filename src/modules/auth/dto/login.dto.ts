import { IsEmail, IsString, IsNotEmpty, MinLength } from "class-validator";

export class LogInDto {
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(4)
  password: string;

  @IsString()
  @IsNotEmpty()
  client_id: string;
}
