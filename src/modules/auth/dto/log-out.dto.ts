import { IsString, IsNotEmpty, IsOptional } from 'class-validator';

export class LogOutDto {
  @IsString()
  @IsNotEmpty()
  access_token: string;

  @IsString()
  @IsNotEmpty()
  client_id: string;

  @IsString()
  @IsOptional()
  user_id?:number
}
