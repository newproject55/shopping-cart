import {
  Controller,
  Post,
  Body,
  HttpCode,
  HttpStatus,
  UseGuards,
  BadRequestException,
  Delete,
  Req,
} from "@nestjs/common";
import { LocalAuthGuard } from "./guard/local-auth.guard";
import { AuthService } from "./auth.service";
import { formatResponse } from "src/utils/helpers/formatResponse";
import { RegisterDto } from "./dto/register.dto";
import { LogInDto } from './dto/login.dto';
import { LogOutDto } from "./dto/log-out.dto";
import { JwtAuthGuard } from "./guard/jwt-auth.guard";
import { UserRole } from 'src/utils/enum/user-role.enum';
import { RefreshTokenDto } from "./dto/refresh-token.dto";
import { VerifyEmailDto } from './dto/verify-infor.dto';
import { ForgotPasswordDto } from "./dto/forgot-password.dto";
import { ResetPasswordDto } from "./dto/reset-password.dto";
import { RequestWithUser } from "src/utils/interface/request-with-user.interface";
import { ApiTags } from "@nestjs/swagger";

@ApiTags("Auth")
@Controller("auth")
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post("register")
  @HttpCode(201)
  async register(@Body() registerDto: RegisterDto) {
    try {
      if (registerDto.roles) registerDto.roles = [UserRole.USER];
      const createdUser = await this.authService.register(registerDto);
      if (!createdUser) {
        throw new BadRequestException("Register failed!");
      }
      return formatResponse(HttpStatus.CREATED, "Register successfully!", createdUser);
    } catch (error) {
      return formatResponse(error.status, error.message);
    }
  }

  @UseGuards(LocalAuthGuard)
  @Post("log-in")
  async login(@Body() loginDto: LogInDto) {
    try {
      const data = await this.authService.login(loginDto);
      if (!data) {
        throw new BadRequestException("Log-in failed!");
      }
      return formatResponse(HttpStatus.OK, "Log-in successfully!", data);
    } catch (error) {
      return formatResponse(error.status, error.message);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Delete("log-out")
  async logout(@Body() logOutDto: LogOutDto) {
    try {
      await this.authService.logout(logOutDto);
      return formatResponse(HttpStatus.OK, "Log-out successfully!");
    } catch (error) {
      return formatResponse(error.status, error.message);
    }
  }

  /*
  @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN))
  @Delete("log-out/:id")
  async logoutByAdmin(@Body() logOutDto: LogOutDto, @Param('id') id: number) {
    try {
      await this.authService.logout(logOutDto);
      return formatResponse(HttpStatus.OK, "Log-out successfully!");
    } catch (error) {
      return formatResponse(error.status, error.message);
    }
  }
  */

  @Post("forgot-password")
  async forgotPassword(@Body() { email }: ForgotPasswordDto) {
    try {
      await this.authService.sendCodeForgotPassword(email);
      return formatResponse(HttpStatus.OK, 'Send code forgot password successfully!');
    } catch (error) {
      return formatResponse(error.status, error.message);
    }
  }

  @Post("reset-password")
  async resetPassword(@Req() request: RequestWithUser, @Body() { code }: ResetPasswordDto) {
    try {
      const userId = request.user.id;
      await this.authService.confirmCodeForgotPassword(userId, code);
      return formatResponse(HttpStatus.OK, 'Reset password successfully!');
    } catch (error) {
      return formatResponse(error.status, error.message);
    }
  }

  @Post("refresh-token")
  async refreshToken(@Body() { refresh_token, client_id }: RefreshTokenDto) {
    try {
      const data = await this.authService.refreshToken(refresh_token, client_id);
      return formatResponse(HttpStatus.OK, "Refresh token successfully!", data);
    } catch (error) {
      return formatResponse(error.status, error.message);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Post("send-verify-email")
  async sendEmailVerifyEmail(@Req() request: RequestWithUser) {
    try {
      const userId = request.user.id;
      await this.authService.sendEmailVerify(userId);
      return formatResponse(HttpStatus.OK, "Send email successfully!");
    } catch (error) {
      return formatResponse(error.status, error.message);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Post("verify-email")
  async confirmEmail(@Body() { verify_email_token }: VerifyEmailDto) {
    try {
      await this.authService.confirmEmail(verify_email_token);
      return formatResponse(HttpStatus.OK, "Verify email successfully!");
    } catch (error) {
      return formatResponse(error.status, error.message);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Post("send-verify-contact")
  async sendSmsVerifyContact(@Req() request: RequestWithUser) {
    try {
      const phoneNumber = request.user.phone_number;
      const code = await this.authService.sendSmsVerifyPhoneNumber(phoneNumber);
      return formatResponse(HttpStatus.OK, "Send sms successfully!",code);
    } catch (error) {
      return formatResponse(error.status, error.message);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Post("verify-contact")
  async confirmContact(@Req() request: RequestWithUser, @Body() { code } : ResetPasswordDto) {
    try {
      const userId = request.user.id;
      await this.authService.confirmContact(userId, code);
      return formatResponse(HttpStatus.OK,"Verify contact successfully!")
    } catch (error) {
      return formatResponse(error.status, error.message);
    }
  }
}