import {
  Injectable,
  HttpException,
  BadRequestException,
  InternalServerErrorException
} from "@nestjs/common";
import { UsersService } from "../users/users.service";
import { RegisterDto } from "./dto/register.dto";
import { comparePassword } from "src/utils/bcrypt/comparePassword";
import { LogInDto } from "./dto/login.dto";
import { TokensService } from "../tokens/tokens.service";
import { ConfigService } from "@nestjs/config";
import { LogOutDto } from './dto/log-out.dto';
import { VerificationTokenPayload } from "src/utils/interface/verification-token-payload.interface";
import { EmailService } from "../email/email.service";
import { SmsService } from '../sms/sms.service';
import { randomNumberCode } from "src/utils/helpers/random-code-verify";
import { generatePassword } from "src/utils/helpers/generate-password";

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly configService: ConfigService,
    private readonly tokensService: TokensService,
    private readonly emailService: EmailService,
    private readonly smsService: SmsService
  ) {}

  async register(registerDto: RegisterDto) {
    try {
      const newUser = await this.usersService.createUser(registerDto);
      const { password, ...result } = newUser;
      return result;
    } catch (error) {
      throw new HttpException(error.message, error.status);
    }
  }

  async login(loginDto: LogInDto) {
    try {
      const user = await this.validateUser(loginDto.email, loginDto.password);
      const tokens = this.tokensService.generateTokens({
        id: user.id,
        email: user.email,
        roles: user.roles
      });
      await this.tokensService.saveToken({
        access_token: tokens.access_token,
        refresh_token: tokens.refresh_token,
        userId: user.id,
        client_id: loginDto.client_id
      });
      return [
        {
          access_token: tokens.access_token,
          expireAfter: this.configService.get("JWT_ACCESS_TOKEN_EXPIRATION_TIME")
        },
        {
          refresh_token: tokens.refresh_token,
          expireAfter: this.configService.get("JWT_REFRESH_TOKEN_EXPIRATION_TIME")
        }
      ]
    } catch (error) {
      throw new HttpException(error.message, error.status);
    }
  }

  async logout(LogOutDto: LogOutDto) {
    try {
      await this.tokensService.deleteToken(LogOutDto);
    } catch (error) {
      throw new HttpException(error.message, error.status);
    }
  }

  async refreshToken(refreshToken: string, client_id: string) {
    try {
      const newToken = await this.tokensService.generatNewTokens(refreshToken, client_id);
      await this.tokensService.saveToken({
        access_token: newToken.token.access_token,
        refresh_token: newToken.token.refresh_token,
        userId: newToken.id,
        client_id: client_id
      });
      return [
        {
          access_token: newToken.token.access_token,
          expireAfter: this.configService.get("JWT_ACCESS_TOKEN_EXPIRATION_TIME")
        },
        {
          refresh_token: newToken.token.refresh_token,
          expireAfter: this.configService.get("JWT_REFRESH_TOKEN_EXPIRATION_TIME")
        }
      ]
    } catch (error) {
      throw new HttpException(error.message, error.status);
    }
  }

  async validateUser(email: string, pass: string): Promise<any> {
    try {
      const user = await this.usersService.getUserByEmail(email);
      if (user) {
        const matched = await comparePassword(pass, user.password);
        if (matched) {
          const { password, ...result } = user;
          return result;
        } else {
          return null;
        }
      }
    } catch (error) {
      throw new BadRequestException("Wrong credentials provided");
    }
  }

  async sendCodeForgotPassword(email: string) {
    try {
      const user = await this.usersService.getUserByEmail(email);
      const code = randomNumberCode(6);
      const payload: VerificationTokenPayload = { code }
      const token = this.tokensService.generateVerificationToken(payload);
      if (!token) {
        throw new InternalServerErrorException("Generate token failed!")
      }
      await this.usersService.saveForgotPasswordToken(user.id, token);
      return await this.emailService.sendEmail({
        to: email,
        subject: 'Shopping-Cart! Forgot-password',
        text: `Your reset password code: ${code}`
      })
    } catch (error) {
      throw new HttpException(error.message, error.status);
    }
  }

  async confirmCodeForgotPassword(userId: number, code: string) {
    try {
      const user = await this.usersService.getUserById(userId);
      const check = await this.tokensService.verifyForgotPasswordToken(user.forgot_password_token, code);
      if (check === true) {
        const newPassword = generatePassword();
        await this.usersService.resetPassword(user.id, newPassword);
        return await this.emailService.sendEmail({
          to: user.email,
          subject: 'Shopping-cart! Reset-password',
          text: `Your new password: ${newPassword}`
        })
      }
    } catch (error) {
      throw new HttpException(error.message, error.status);
    }
  }

  async sendEmailVerify(userId: number) {
    try {
      const user = await this.usersService.getUserById(userId);
      if (user.verify_email === true) {
        throw new BadRequestException('Email already confirmed!');
      }
      const email = user.email;
      const tokenVerify = this.tokensService.generateVerificationToken({ email });
      await this.usersService.saveVerificationEmailToken(user.id, tokenVerify);
      const url = `${this.configService.get('EMAIL_CONFIRMATION_URL')}?token=${tokenVerify}`;
      const content = `Wellcome to the application. To verify the email address, click here: ${url}`;
      return await this.emailService.sendEmail({
        to: user.email,
        subject: 'Shopping-Cart! Verification Yours Email',
        text: content
      });
    } catch (error) {
      throw new HttpException(error.message, error.status);
    }
  }

  async sendSmsVerifyPhoneNumber(phoneNumber: string) {
    try {
      const user = await this.usersService.getUserByPhoneNumber(phoneNumber);
      if (user.verify_contact === true) {
        throw new BadRequestException('Phone number already confirmed');
      }
      const code = await this.smsService.initialSmsPhoneNumberVerification(phoneNumber);
      const token = this.tokensService.generateVerificationToken({ code });
      await this.usersService.saveVerifyContactToken(user.id, token);
      return code;
    } catch (error) {
      console.log(error)
      throw new HttpException(error.message, error.status);
    }
  }

  async confirmEmail(verifyEmailToken: string) {
    try {
      const email = await this.emailService.decodeVerifyEmailToken(verifyEmailToken);
      await this.emailService.verifyEmail(email);
    } catch (error) {
      throw new HttpException(error.message, error.status);
    }
  }

  async confirmContact(userId: number, code: string) {
    try {
      const user = await this.usersService.getUserById(userId);
      const payloadToken = this.tokensService.decodeToken(user.verify_contact_token, this.configService.get('JWT_VERIFICATION_TOKEN_SECRET'));
      const payloadContent = Object.values(payloadToken)[0];
      const codePayload = Object.values(payloadContent)[0];
      if (codePayload === code) {
        return await this.usersService.activeContact(user.id);
      }
      throw new BadRequestException("Verify contact failed");
    } catch (error) {
      throw new HttpException(error.message, error.status);
    }
  }
}
