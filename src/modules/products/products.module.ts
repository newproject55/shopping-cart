import { Module } from '@nestjs/common';
import { ProductsService } from './products.service';
import { ProductsController } from './products.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import products from './entity/products.entity';
import { CloudinaryModule } from 'src/cloudinary/cloudinary.module';
import { ImagesProductModule } from '../images-product/images-product.module';
import products_of_cart from '../products-of-cart/entity/products-of-cart.entity';
import category from '../category/entity/category.entity';
import products_of_category from '../products-of-category/entity/products-of-category.entity';
import reviews from '../reviews/entity/reviews.entity';


@Module({
  imports: [
    TypeOrmModule.forFeature([products,products_of_cart,category,products_of_category,reviews]),
    CloudinaryModule,
    ImagesProductModule,
  ],
  providers: [ProductsService],
  controllers: [ProductsController],
  exports: [ProductsService]
})
export class ProductsModule {}
