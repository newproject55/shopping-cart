import {
    Controller,
    Get,
    Post,
    Body,
    Patch,
    Param,
    Delete,
    HttpStatus,
    Query,
    UseGuards,
    Res
} from "@nestjs/common";
import { CreateProductDto } from "./dto/create-product.dto";
import { ProductsService } from "./products.service";
import { CloudinaryService } from "src/cloudinary/cloudinary.service";
import { formatResponse } from "src/utils/helpers/formatResponse";
import { JwtAuthGuard } from "../auth/guard/jwt-auth.guard";
import RoleGuard from "../auth/guard/auth.guard";
import { UserRole } from "src/utils/enum/user-role.enum";
import { UpdateProductDto } from "./dto/update-product.dto";
import { PaginationQueryDto } from "src/utils/dto/pagination-query.dto";
import { ProductsQueryParams } from "./dto/query-products.dto";
import { Workbook } from "exceljs";

@Controller('products')
export class ProductsController {
    constructor(
        private readonly productsService: ProductsService,
        private readonly cloudinaryService: CloudinaryService
    ) {}

    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN))
    @Post('add')
    async addProduct(@Body() createProduct: CreateProductDto) {
        try {
            const newProduct = await this.productsService.createProduct(createProduct)
            return formatResponse(
                HttpStatus.OK,
                'Create Product Successfully!',
                newProduct
            )
        } catch (error) {
            await this.cloudinaryService.removeImage(createProduct.thumbnail_img_name)
            const a = await Promise.all(createProduct.images_name.map(async name => {
                return new Promise(async (resolve, reject) => {
                    const deleteImage = await this.cloudinaryService.removeImage(name);
                    resolve(deleteImage);
                })
            }
            ));
            return formatResponse(
                error.status || HttpStatus.INTERNAL_SERVER_ERROR,
                error?.message || error
            )
        }
    }

    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN))
    @Get('export-excel-file')
    async exportExcelFile(@Res() response: any, @Query() { page, size }: PaginationQueryDto) {
        try {
            const { list } = await this.productsService.getAll(page, size);
            //Create a sheet
            const workbook = new Workbook();
            const workSheet = workbook.addWorksheet('List of products');
            //Define the columns header
            const productsColumns = [
                { key: 'id', header: 'STT' , width: 5 },
                { key: 'name', header: 'Name' , width: 15},
                { key: 'name_search', header: 'Name search' , width: 15},
                { key: 'description', header: 'Description' , width: 30},
                { key: 'price', header: 'Price' , width: 10},
                { key: 'discount_percent', header: 'Discount percent' , width: 20},
                { key: 'brand', header: 'Brand' , width: 25},
                { key: 'brand_search', header: 'Brand search' , width: 25},
                { key: 'stock_quantity', header: 'Stock' , width: 10},
                { key: 'rate_avg_review', header: 'Rating' , width: 10},
                { key: 'thumbnail_image', header: 'Thumbnail image' , width: 25},
                { key: 'thumbnail_img_name', header: 'Thumbnail image name' , width: 25},
                { key: 'created_at', header: 'Create at' , width: 25},
                { key: 'updated_at', header: 'Updated at' , width: 25}
            ]
            workSheet.columns = productsColumns;
            //Write the data to the file
            list.forEach((product) => {
                workSheet.addRow(product);
            });
            workSheet.columns.forEach((sheetColumn) => {
                sheetColumn.font = {
                    size: 13,
                }
            });

            workSheet.getRow(1).font = {
                bold: true,
                size: 13,
            };
            //export file
            const filename = 'products.xlsx';
            const pathx = 'D:/FPT/shopping-cart/excel-file/';
            const exportPath = pathx + filename;
            response.setHeader(
                "Content-Type",
                "application/json; charset=utf-8"
            );
            response.setHeader(
                "Content-Disposition",
                "attachment; filename=" + "products.xlsx"
            );
            await workbook.xlsx.writeFile(exportPath);
            await workbook.xlsx.write(response);
            response.sendFile(filename, function (err) {
                console.log('error:', err);
            });
        } catch (error) {
            return formatResponse(
                error.status || HttpStatus.INTERNAL_SERVER_ERROR,
                error?.message || error
            )
        }
    }

    @Get('discount-percent')
    async getDiscountPercent(@Query() { page, size }: PaginationQueryDto) {
        try {
            const result = await this.productsService.productsBiggestDiscount(page, size)
            return formatResponse(
                HttpStatus.OK,
                'get by discount-percent successfully',
                result
            )
        } catch (error) {
            return formatResponse(
                error.status || HttpStatus.INTERNAL_SERVER_ERROR,
                error?.message || error
            )
        }
    }

    @Get("list")
    async getAllProducts(@Query() { page, size }: PaginationQueryDto) {
        try {
            const allProduct = await this.productsService.getAll(page, size);
            return formatResponse(HttpStatus.OK, "Successfully!", allProduct);
        } catch (error) {
            return formatResponse(error.status, error.message);
        }
    }

    @Get()
    async findAll(
        @Query() { page, size, order, sortField, keyWord }: ProductsQueryParams,
    ) {
        try {
            const listSearchProducts = await this.productsService.searchProducts(
                page,
                size,
                order,
                sortField,
                keyWord
            )
            return formatResponse(
                HttpStatus.OK,
                'Search products success!',
                listSearchProducts,
            );
        } catch (error) {
            return formatResponse(
                error.status || HttpStatus.INTERNAL_SERVER_ERROR,
                error?.message || error
            )
        }
    }

    @Get(':id')
    async getProductById(@Param('id') id: number) {
        try {
            const product = await this.productsService.getProductById(id)
            return formatResponse(
                HttpStatus.OK,
                'Get Product Successfully!',
                product
            )
        } catch (error) {
            return formatResponse(
                error.status || HttpStatus.INTERNAL_SERVER_ERROR,
                error?.message || error
            )
        }
    }

    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN))
    @Patch(':id')
    async updateProduct(@Param('id') id: number, @Body() updateProduct: UpdateProductDto) {
        try {
            const update = await this.productsService.updateProduct(id, updateProduct)
            return update
        } catch (error) {
            return formatResponse(
                error.status || HttpStatus.INTERNAL_SERVER_ERROR,
                error?.message || error
            )
        }
    }

    @UseGuards(JwtAuthGuard, RoleGuard(UserRole.ADMIN))
    @Delete(':id')
    async deleteProduct(@Param('id') id: number) {
        try {
            await this.productsService.deleteProduct(id);
            return formatResponse(
                HttpStatus.OK,
                'Delete product successfully!'
            )
        } catch (error) {
            return formatResponse(
                error.status || HttpStatus.INTERNAL_SERVER_ERROR,
                error?.message || error
            )
        }
    }
}
