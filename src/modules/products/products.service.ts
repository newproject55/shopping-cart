import { BadRequestException, HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { OrderQueryParams } from 'src/utils/enum/order-query-param.enum';
import { removeVietnameseAccent } from 'src/utils/helpers/charactor';
import { formatResponse } from 'src/utils/helpers/formatResponse';
import { getPagination } from 'src/utils/helpers/pagination';
import { FindManyOptions, Like, Repository } from 'typeorm';
import category from '../category/entity/category.entity';
import { ImagesProductService } from '../images-product/images-product.service';
import products_of_cart from '../products-of-cart/entity/products-of-cart.entity';
import products_of_category from '../products-of-category/entity/products-of-category.entity';
import reviews from '../reviews/entity/reviews.entity';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import products from './entity/products.entity';

@Injectable()
export class ProductsService {
    constructor(
        @InjectRepository(products)
        private productsRepository: Repository<products>,
        @InjectRepository(products_of_cart)
        private productOfCartRepository: Repository<products_of_cart>,
        @InjectRepository(category)
        private categoryRepository: Repository<category>,
        @InjectRepository(products_of_category)
        private productsOfCategoryRepository: Repository<products_of_category>,
        @InjectRepository(reviews)
        private reviewsRepository: Repository<reviews>,
        private readonly imagesProductService: ImagesProductService,
    ) {}

    async createProduct(createProduct: CreateProductDto) {
        try {
            const newProduct = this.productsRepository.create(createProduct)
            const saveProduct = await this.productsRepository.save(newProduct)
            await Promise.all(createProduct.category.map( async(category) => {
                return new Promise( async(resolve, reject) => {
                    const findCategory = await this.categoryRepository.findOne({
                        where: {name: category}
                    })
                    const productsofcategory = {
                        products: saveProduct,
                        category: findCategory
                    }
                    const createProductOfCategory = this.productsOfCategoryRepository.create(productsofcategory)
                    await this.productsOfCategoryRepository.save(createProductOfCategory)
                    resolve('success')
                })
            }))
            createProduct.images.forEach(async (image,index) => {
                await this.imagesProductService.saveImage(image,createProduct.images_name[index],saveProduct)
            })
            if (newProduct && saveProduct) {
                return saveProduct
            }
        } catch (error) {
            throw new HttpException(
                error?.message||error,
                error.status||HttpStatus.INTERNAL_SERVER_ERROR
            )
        }
    }

    async getProductById(id: number) {
        try {
            const findProduct = await this.productsRepository.findOne({
                where: { id },
                select: {
                    id: true, name: true, brand: true, thumbnail_image: true,
                    images_product: { id: true, link_img: true },
                    discount_percent: true, description: true,
                    price: true, rate_avg_review: true,
                    spec: true, stock_quantity: true
                },
                relations: { images_product: true }
            });
            if (!findProduct) {
                throw new BadRequestException('Not found product')
            }
            return findProduct;
        } catch (error) {
            throw new HttpException(error.message, error.status)
        }
    }

    async productsBiggestDiscount(page: number, size: number) {
        try {
            const { limit, offset } = getPagination(page, size);
            const [list, count] = await this.productsRepository.findAndCount({
                order: { ['discount_percent']: 'DESC' },
                skip: offset,
                take: limit
            })
            if (list.length === 0) {
                throw new BadRequestException("Not found product in cart");
            }
            return { list, count, page, size };
        } catch (error) {
            throw new HttpException(error.message, error.status)
        }
    }

    async updateProduct(id: number, updateProduct: UpdateProductDto) {
        try {
            if (updateProduct?.name) {
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore
                updateProduct.name_search = removeVietnameseAccent(
                    updateProduct.name,
                );
            }
            if (updateProduct?.brand) {
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore
                updateProduct.brand_search = removeVietnameseAccent(
                    updateProduct.brand,
                );
            }
            const {images, images_name, category,...products} = updateProduct
            await this.productsRepository.update(id,products)
            const findProduct = await this.productsRepository.findOne({where: {id}})
            if (!findProduct) {
                throw new BadRequestException('Not found product')
            }
            await this.productsRepository.save(findProduct);
            const findAllProduct = await this.findAllProductOfCartByProductId(id)
            const result = await Promise.all(findAllProduct.map(async (product) => {
                return new Promise(async (resolve, reject) => {
                    const newProductOfCart = {
                        price: product.product.price,
                        total_price: product.product.price * product.quantity
                    }
                    const updateProduct = this.productOfCartRepository.create(newProductOfCart)
                    await this.productOfCartRepository.update(product.id, updateProduct)
                    resolve('successfully')
                })
            }))
            if(updateProduct?.category){
                const findCategory = await this.productsOfCategoryRepository.find({
                    where: {products: {id}},
                    relations: ['products']
                })
                await this.productsOfCategoryRepository.remove(findCategory)
                await Promise.all(updateProduct.category.map( async(category) => {
                    return new Promise( async(resolve, reject) => {
                        const findCategory = await this.categoryRepository.findOne({
                            where: {name: category}
                        })
                        const productsofcategory = {
                            products: findProduct,
                            category: findCategory
                        }
                        const createProductOfCategory = this.productsOfCategoryRepository.create(productsofcategory)
                        await this.productsOfCategoryRepository.save(createProductOfCategory)
                        resolve('success')
                    })
                }))
            }
            if(updateProduct?.images){
                await this.imagesProductService.deleteImageByProductId(id)
                await Promise.all(updateProduct.images.map(async (element, index) => {
                    return new Promise(async (resolve, reject) => {
                        await this.imagesProductService.saveImage(element, images_name[index], findProduct)
                        resolve('successfully!')
                    })
                }))
            }
            return formatResponse(HttpStatus.OK, 'Update product is successfully')
        } catch (error) {
            throw new HttpException(
                error?.message || error,
                error?.status || HttpStatus.INTERNAL_SERVER_ERROR
            )
        }
    }

    async deleteProduct(id: number) {
        try {
            const findProduct = await this.productsRepository.findOne({ where: { id } });
            if (!findProduct) return new BadRequestException('Not Found Product!');
            const deleteProduct = await this.productsRepository.remove(findProduct);
            return deleteProduct;
        } catch (error) {
            throw new HttpException(
                error?.message || error,
                error.status
            )
        }

    }

    async findAllProductOfCartByProductId(id: number) {
        try {
            const products = await this.productOfCartRepository.find({
                where: { product: { id } },
                relations: { product: true }
            })
            return products
        } catch (error) {
            throw new HttpException(error.message, error.status)
        }
    }

    async getAll(page: number, size: number) {
        try {
            const { limit, offset } = getPagination(page, size);
            const [list, count] = await this.productsRepository.findAndCount({
                order: { ['id']: 'DESC' },
                skip: offset,
                take: limit
            })
            if (list.length === 0) {
                throw new BadRequestException("Not found product in cart");
            }
            return { list, count, page, size };
        } catch (error) {
            throw new HttpException(error.message, error.status)
        }
    }

    async searchProducts(
        page?: number,
        size?: number,
        order?: OrderQueryParams,
        sortField?: string,
        keyWord?: string,
      ){
        try {
            keyWord = keyWord ? removeVietnameseAccent(keyWord) : '';
            const where: FindManyOptions<products>['where'] = [
                { name_search: Like(`%${keyWord}%`) }, 
                { brand_search: Like(`%${keyWord}%`)}
            ];
            const { limit, offset } = getPagination(page, size);
            const data = await this.productsRepository.findAndCount({
                where,
                order: {
                    [sortField || 'id']: order || 'ASC',
                },
                skip: offset,
                take: limit
            });
            return {data, page, limit};
        } catch (error) {
            throw new HttpException(
                error?.message||error,
                error?.status||HttpStatus.INTERNAL_SERVER_ERROR
            )
        }
        
      }

    async updateRatingProduct(productId: number,currentRating: number) {
        await this.productsRepository.update({ id: productId}, {
            rate_avg_review: currentRating
        })
    }

    async insertDataFormExcel() {

    }
}
