import { Exclude } from 'class-transformer';
import images_product from 'src/modules/images-product/entity/images-product.entity';
import products_of_category from 'src/modules/products-of-category/entity/products-of-category.entity';
import reviews from 'src/modules/reviews/entity/reviews.entity';
import { removeVietnameseAccent } from 'src/utils/helpers/charactor';
import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    UpdateDateColumn,
    JoinColumn,
    OneToMany,
    Index,
    BeforeInsert,
    BeforeUpdate
} from 'typeorm';

@Entity('products')
export default class products {
    @PrimaryGeneratedColumn()
    public id?: number;
  
    @Column()
    public name: string;

    @Index('name_search_product_index')
    @Column({ nullable: true })
    @Exclude()
    public name_search: string;
  
    @Column()
    public description: string;

    @Column({type: 'float'})
    public price : number;

    @Column({type: 'float'})
    public discount_percent: number;

    @Column()
    public brand: string;

    @Index('search_brand_index')
    @Column({ nullable: true })
    @Exclude()
    public brand_search: string;

    @Column()
    public stock_quantity: number;

    @Column({type: "float"})
    public rate_avg_review: number ;

    @Column('text',{nullable:true, default: null})
    public spec: string;

    @Column()
    public thumbnail_image: string

    @Column()
    public thumbnail_img_name: string

    @OneToMany(() => images_product, (images_product) => images_product.product, { cascade: true })
    @JoinColumn()
    public images_product: images_product[]

    @OneToMany(() => products_of_category, products_of_category => products_of_category.products, { cascade: true })
    public categories: products_of_category[]

    @OneToMany(() => reviews, (reviews) => reviews.users, { cascade: true })
    public reviews: reviews[]

    @CreateDateColumn({
      type: 'timestamp',
      default: () => 'CURRENT_TIMESTAMP(6)',
    })
    public created_at: Date;
  
    @UpdateDateColumn({
      type: 'timestamp',
      default: () => 'CURRENT_TIMESTAMP(6)',
      onUpdate: 'CURRENT_TIMESTAMP(6)',
    })
    public updated_at: Date;

    @BeforeInsert()
    async beforeInsert() {
      this.name_search = removeVietnameseAccent(this.name);
      this.brand_search = removeVietnameseAccent(this.brand);
    }
    @BeforeUpdate()
    beforeUpdateProduct() {
      this.name_search = removeVietnameseAccent(this.name);
      this.brand_search = removeVietnameseAccent(this.brand);
    }
}