import {
    IsString,
    IsNotEmpty,
    IsOptional,
    IsNumber,
    IsArray,
    Max,
    Min
  } from "class-validator";

export class CreateProductDto {
    @IsString()
    @IsNotEmpty()
    name: string;
  
    @IsString()
    @IsNotEmpty()
    description: string;
  
    @IsNumber()
    @IsNotEmpty()
    price: number;
  
    @IsNumber()
    @Max(99)
    @Min(0)
    @IsNotEmpty()
    discount_percent: number;

    @IsString()
    @IsNotEmpty()
    brand: string;
  
    @IsNumber()
    @IsNotEmpty()
    stock_quantity: number;
    
    @IsString()
    @IsNotEmpty()
    thumbnail_image: string;

    @IsString()
    @IsNotEmpty()
    thumbnail_img_name: string;

    @IsNumber()
    @IsNotEmpty()
    rate_avg_review: number;
    
    @IsString()
    @IsOptional()
    spec: string;  // details

    @IsArray()
    @IsNotEmpty()
    images: string[];

    @IsArray()
    @IsNotEmpty()
    images_name: string[];

    @IsArray()
    @IsNotEmpty()
    category: string[];
}