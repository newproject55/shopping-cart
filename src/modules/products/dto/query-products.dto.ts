import { IsEnum } from 'class-validator';
import { PaginationParamsDto } from 'src/utils/dto/paginationParams.dto'; 
import { SortFieldProductsEnum } from 'src/utils/enum/sortFieldProducts.enum';

export class ProductsQueryParams extends PaginationParamsDto {
  @IsEnum(SortFieldProductsEnum)
  sortField?: SortFieldProductsEnum | string;
}

