import {
    IsString,
    IsNotEmpty,
    IsOptional,
    IsNumber,
    IsArray,
    max,
    Max,
    Min
  } from "class-validator";

export class UpdateProductDto {
    @IsString()
    @IsOptional()
    name: string;
  
    @IsString()
    @IsOptional()
    description: string;
  
    @IsNumber()
    @IsOptional()
    price: number;
  
    @IsNumber()
    @Max(99)
    @Min(0)
    @IsOptional()
    discount_percent: number;

    @IsString()
    @IsOptional()
    brand: string;
  
    @IsNumber()
    @IsOptional()
    stock_quantity: number;
    

    @IsNumber()
    @IsOptional()
    rate_avg_review: number;
    
    @IsString()
    @IsOptional()
    spec: string;  // details

    @IsString()
    @IsOptional()
    thumbnail_image: string;

    @IsString()
    @IsOptional()
    thumbnail_img_name: string;

    @IsArray()
    @IsOptional()
    images: string[];

    @IsArray()
    @IsOptional()
    images_name: string[];

    @IsArray()
    @IsNotEmpty()
    category: string[];
}